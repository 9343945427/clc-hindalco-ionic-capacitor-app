import { Injectable } from '@angular/core';
import { ToastController,LoadingController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';

@Injectable({
	providedIn: 'root'
})
export class LoadingService {

	constructor(
		public loadingCtrl: LoadingController,
		public toastCtrl: ToastController,
		public alertController: AlertController
	){

	}

	async open(message?) {
		let loading = await this.loadingCtrl.create({
			message: message ? message : 'Please wait ...',
			mode:'ios',
    	});
    	await loading.present();
	}

	async close() {
		await this.loadingCtrl.dismiss()
	}

	async message(message) {
		const toast = await this.toastCtrl.create({
			message: message,
			duration: 2000,
			position: 'bottom',
			color:'dark',
			translucent:true
		});
		toast.present();
	}

	async presentAlert(title,message) {
		const alert = await this.alertController.create({
			cssClass: 'alert',
			header: title,
			message: message,
			buttons: ['OKAY']
		});
		await alert.present();
  	}

	async presentLoading() {
		const loading = await this.loadingCtrl.create({
		  	message: 'Please wait... Data sync is in progress',
		  	duration: 5000
		});
		await loading.present();
	}
}
