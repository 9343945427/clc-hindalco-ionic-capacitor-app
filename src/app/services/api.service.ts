import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { LoadingService } from './login.service';
import { Storage } from '@ionic/storage';
import { environment } from 'src/environments/environment.prod';
@Injectable({
	providedIn: 'root'
})
export class ApiService {
	url = environment.apiUrl
	users: any;
	can_leave=false
	constructor(
		public loading: LoadingService,
		private http: HttpClient,
		private storage: Storage,
		private navCtrl: NavController,
		public toastController: ToastController,

	) { }
	open(uri, data) {
		return new Promise((resolve, reject) => {
			this.http.post(this.url + uri, data)
				.subscribe(response => {
					resolve(response);
				}, (error) => {
					if (error.error.message == 'Too Many Attempts.') {
						let errors = { errors: { email: ['Too Many Attempts, Login after sometimes'] } };
						reject(errors);
					}
					else {
						reject(error.error);
					}
				});
		});
	}

	httpCall(uri, data, token) {
		return new Promise((resolve, reject) => {
			this.http.post(this.url + uri, data, {
				headers: new HttpHeaders().set('Authorization', 'Bearer' + ' ' + token)
			})
				.subscribe(response => {
					resolve(response);
				},
					(error) => {
						if (error.error.message == 'Unauthenticated.') {
							this.storage.remove('user');
							this.navCtrl.navigateRoot('/');
							this.presentToast()
							let errors = { errors: { email: ['Unauthenticated.'] } };
							reject(errors);
						}
						else {
							reject(error.error);
						}
					});
		});
	}

	async presentToast() {
		const toast = await this.toastController.create({
			message: 'Unauthenticated. Please Login.',
			duration: 1000,
		});
		toast.present();
	}
}
