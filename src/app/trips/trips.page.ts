import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { LoadingService } from '../services/login.service';
import { Storage } from '@ionic/storage';
import { ModalController } from '@ionic/angular';
import { ViewPage } from './view/view.page';

@Component({
	selector: 'app-trips',
	templateUrl: './trips.page.html',
	styleUrls: ['./trips.page.scss'],
})
export class TripsPage implements OnInit {

	user: any;
	token: any;
	meta = {
		keyword: "trip_id",
		search: "",
		order_by: "DESC",
		per_page: 10,
		type: "new",
		status: 1,
		page: 1,
		last_page: 0,
	}
	trips: any;
	constructor(
		private api: ApiService,
		private loading: LoadingService,
		private storage: Storage,
		private modalCtrl: ModalController,
	) { }

	ngOnInit() { }

	async ionViewWillEnter() {
		await this.storage.get('user').then((data) => {
			if (data != null) {
				this.user = data;
			}
		});
		await this.storage.get('token').then((data) => {
			if (data != null) {
				this.token = data;
				this.paginateTrips()
			}
		});
	}

	async paginateTrips(e?) {
		let payload = {
			keyword: "trip_id",
			search: this.meta.search,
			order_by: this.meta.order_by,
			per_page: 10,
			type: this.meta.type,
			status: this.meta.status,
			page: this.meta.page,
			last_page: this.meta.last_page,
		}
		await this.api.httpCall('paginateTrips', payload, this.token).then(res => {
			this.meta.last_page = res['meta'].last_page;
			if (this.meta.type == 'new')
				this.trips = res['data']
			else
				this.trips = this.trips.concat(res['data']);
			if (e)
				e.target.complete();
		}, err => {
			e.target.complete();
			console.log(err)
		})
	}

	async viewTrip(trip) {
		const modal = await this.modalCtrl.create({
			component: ViewPage,
			cssClass:'fullscreen',
			componentProps: {
				trip: trip
			}
		});
		modal.present();
	}

	async loadData(event) {
		this.meta.type = 'append';
		this.meta.page++;
		await this.paginateTrips(event);
	}

	async doRefresh(event) {
		this.meta.type = 'new';
		this.meta.page = 1;
		this.meta.last_page = 0
		this.trips = null;
		await this.paginateTrips(event);
	}

	async searchData(event) {
		if (event.key == 'Enter' || event.target.value == '') {
			this.meta.type = 'new';
			this.meta.page = 1;
			this.meta.last_page = 0;
			this.meta.search = event.target.value;
			await this.paginateTrips();
		}
	}
}
