import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import * as moment from 'moment';
import { Storage } from '@ionic/storage';
import { ApiService } from 'src/app/services/api.service';
@Component({
	selector: 'app-view',
	templateUrl: './view.page.html',
	styleUrls: ['./view.page.scss'],
})
export class ViewPage implements OnInit {
	trip: any
	trip_stages: any;
	selected: any = 0;
	trip_checks: any;
	stage_name = ''
	segment = 'info'
	token: any;
	trip_attachments: any;
	trip_check_attachments: any;
	constructor(
		private modalCtrl: ModalController,
		private navParams: NavParams,
		private storage: Storage,
		private api: ApiService,
	) { }

	ngOnInit() {
		this.storage.get('token').then((data) => {
			if (data != null) {
				this.token = data;
			}
		});
		this.trip = this.navParams.get('trip')
		this.trip_stages = this.trip.trip_stages
		this.segment = 'info'
		this.stage_name = 'info'
	}

	async checkSegment(e) {
		this.segment = e.detail.value
		this.stage_name = e.detail.value
	}

	async getCompletedChecks(trip_stage_id) {
		let payload = {
			trip_stage_id: trip_stage_id
		}
		await this.api.httpCall('getTripChecks', payload, this.token).then(res => {
			this.trip_checks = res['data']
			this.trip_attachments = res['trip_attachments']
			this.trip_check_attachments = this.trip_checks.filter(ele => {
				return ele.is_image_required
			})
		}, err => {
			console.log(err)
		})
	}

	convertDateTime(date_time) {
		return moment(date_time).format("DD/MM/YYYY")
	}

	close() {
		this.modalCtrl.dismiss()
	}
}
