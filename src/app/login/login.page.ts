import { Component, OnInit, Version, } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ApiService } from '../services/api.service';
import { LoadingService } from '../services/login.service';
import { Geolocation } from '@capacitor/geolocation';
@Component({
	selector: 'app-login',
	templateUrl: './login.page.html',
	styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
	loginForm: FormGroup;
	errors: any;
	passwordType: string = 'password';
	passwordIcon: string = 'eye-off';
	user_latitude: any;
	user_longitude: any;
	plant_latitude: any;
	plant_longitude: any;
	plant_Radius: any;
	user: any;
	validations = {
		'username': [
			{ type: 'required', message: 'Username field is required.' },
			{ type: 'pattern', message: 'Enter a valid Username.' },
		],
		'password': [
			{ type: 'required', message: 'Password field is required.' },
			{ type: 'minlength', message: 'Password must be at least 8 characters long.' }
		],
	};

	constructor(
		private storage: Storage,
		public router: Router,
		private navCtrl: NavController,
		private api: ApiService,
		private loading: LoadingService
	) {
		this.loginForm = new FormGroup({
			'username': new FormControl('', Validators.compose([
				Validators.required
			])),
			'password': new FormControl('', Validators.compose([
				Validators.minLength(6),
				Validators.required
			])),
		});
	}

	ngOnInit() {

	}

	hideShowPassword() {
		this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
		this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
	}

	async doLogin() {
		await this.loading.open();
		await this.api.open('login', this.loginForm.value).then((response) => {
			this.loading.close();
			this.storage.set('user', response['user']);
			this.storage.set('token', response['token']);
			this.plant_latitude = response['user'].plant.latitude
			this.plant_longitude = response['user'].plant.longitude
			this.plant_Radius = response['user'].plant.plant_radius
			// this.checkLocationPermissions();
			this.navCtrl.navigateRoot('/tabs');
		}, (error) => {
			this.loading.close();
			this.errors = error.errors;
		});
	}

	async checkLocationPermissions() {
		await Geolocation.checkPermissions().then(async res => {
			if (res.coarseLocation !== 'granted' || res.location !== 'granted') {
				await Geolocation.requestPermissions().then(res => {
					if (res.coarseLocation != 'granted' || res.location != 'granted') {
						alert('Please provide all the location permissions')
					} else {
						this.getCurrentCoordinates()
					}
				})
			} else {
				this.getCurrentCoordinates()
			}
		}, err => {
			console.log(err)
			alert('Please switch on GPS / Location to continue..')
		})
	}

	async getCurrentCoordinates() {
		const options = {
			enableHighAccuracy: true,
		}
		await this.loading.open()
		await Geolocation.getCurrentPosition(options).then((resp) => {
			this.loading.close();
			console.log(resp)
			this.user_latitude = resp.coords.latitude;
			this.user_longitude = resp.coords.longitude;
			this.getDistanceFromLatLonInKm(this.user_latitude, this.user_longitude, this.plant_latitude, this.plant_longitude);
		}).catch((error) => {
			console.log('Error getting location', error);
			this.loading.close()
			alert('Please enable app location continue..')
		});
	}

	getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
		var R = 6371000; // Radius of the earth in km
		var dLat = this.deg2rad(lat2 - lat1);  // deg2rad below
		var dLon = this.deg2rad(lon2 - lon1);
		var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		var d = R * c; // Distance in km
		if (this.plant_Radius == "" || this.plant_latitude == "" || this.plant_longitude == "") {
			this.navCtrl.navigateRoot('/tabs');
		}
		else if (d > this.plant_Radius) {
			alert("You are outside of the Plant Radius")
			this.storage.remove('user').then(() => {
				navigator['app'].exitApp()
			}, err => {
				navigator['app'].exitApp()
			});
		}
		else {
			this.navCtrl.navigateRoot('/tabs');
		}
		return d;
	}

	deg2rad(deg) {
		return deg * (Math.PI / 180)
	}
}