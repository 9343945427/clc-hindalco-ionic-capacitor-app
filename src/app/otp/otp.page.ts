import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { LoadingService } from '../services/login.service';
import { NavController } from '@ionic/angular';

@Component({
	selector: 'app-otp',
	templateUrl: './otp.page.html',
	styleUrls: ['./otp.page.scss'],
})
export class OtpPage implements OnInit {
	otp: string = '';
	otp1: string = '';
	otp2: string = '';
	otp3: string = '';
	otp4: string = '';
	otp5: string = '';
	otp6: string = '';
	@ViewChild('Field1') field_1;
	username: any
	constructor(
		public loading: LoadingService,
		public api: ApiService,
		private route: ActivatedRoute,
		public router: Router,
		private navctrl: NavController
	) {
		this.route.queryParams.subscribe(params => {
			if (this.router.getCurrentNavigation().extras.state) {
				this.username = this.router.getCurrentNavigation().extras.state['username'];
			}
		});
	}

	ngOnInit() {
		setTimeout(() => {
			this.field_1.setFocus()
		}, 250)
	}

	gotoNextField(ev: any, next: any, previous: any) {
		if (ev.key !== 'Backspace') {
			next.setFocus()
		} else {
			previous.setFocus()
		}
	}

	async verifyOtp() {
		let payload = {
			username: this.username,
			otp: this.otp1 + this.otp2 + this.otp3 + this.otp4 + this.otp5 + this.otp6
		}
		this.navctrl.navigateForward('/tabs/home');
		// await this.loading.open();
		// await this.api.open('verifyOtp ', payload).then(async (response) => {
		// 	this.loading.close();
		// 	this.kycStatus = response['kyc_status']
		// 	this.storage.set('customer', response['customer']);
		// 	this.storage.set('token', response['token']);
		// 	await this.storage.get('fcm_token').then((data) => {
		// 		if (data != null)
		// 			this.sendToken(data, response['customer'].customer_id)
		// 	})
		// 	this.navCtrl.navigateRoot(['/home']);
		// }, (error) => {
		// 	this.errors = error.error.errors;
		// 	this.loading.close();
		// 	this.otpForm.controls["otp"].setValue(null);
		// 	if (error.error.message == 'Please Register') {
		// 		this.loading.message(error.error.message)
		// 		let navigationExtras: NavigationExtras = {
		// 			state: {
		// 				mobile_no: this.user.mobile_no,
		// 			}
		// 		};
		// 		this.router.navigate(['/email'], navigationExtras);
		// 	}
		// 	else {
		// 		this.loading.presentAlert('Message', error.error.message)
		// 	}
		// });
	}

}