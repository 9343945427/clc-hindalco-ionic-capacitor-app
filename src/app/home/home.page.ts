import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';
@Component({
	selector: 'app-home',
	templateUrl: './home.page.html',
	styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
	user: any;
	token: any;
	meta = {
		keyword: "updated_at",
		search: "",
		order_by: "DESC",
		per_page: 10,
		type: "new",
		status: 0,
		page: 1,
		last_page: 0,
	}
	trips: any;
	constructor(
		private api: ApiService,
		private storage: Storage,
		public router: Router,
	) { }

	async ngOnInit() {
	}

	async ionViewWillEnter() {
		this.meta.type = 'new';
		this.meta.page = 1;
		this.meta.last_page = 0
		this.trips = null;
		await this.storage.get('user').then((data) => {
			if (data != null) {
				this.user = data;
			}
		});
		await this.storage.get('token').then((data) => {
			if (data != null) {
				this.token = data;
				this.meta.page = 1;
				this.meta.last_page = 0;
				this.meta.type = 'new';
				this.paginateTrips();
			}
		});
	}

	async paginateTrips(e?) {
		let payload = {
			keyword: "trip_id",
			search: "",
			order_by: this.meta.order_by,
			per_page: 10,
			type: this.meta.type,
			status: this.meta.status,
			page: this.meta.page,
			last_page: this.meta.last_page,
		}
		await this.api.httpCall('paginateTrips', payload, this.token).then(res => {
			this.meta.last_page = res['meta'].last_page;
			if (this.meta.type == 'new')
				this.trips = res['data']
			else
				this.trips = this.trips.concat(res['data']);
			if (e)
				e.target.complete();
		}, err => {
			e.target.complete();
			console.log(err)
		})
	}

	async loadData(event) {
		this.meta.type = 'append';
		this.meta.page++;
		await this.paginateTrips(event);
	}

	async doRefresh(event) {
		this.meta.type = 'new';
		this.meta.page = 1;
		this.meta.last_page = 0
		this.trips = null;
		await this.paginateTrips(event);
	}

	viewTrip(trip) {
		let navigationExtras: NavigationExtras = {
			state: {
				trip: trip
			}
		}
		this.router.navigate(['/tabs/home/checklist'], navigationExtras)
	}

	convertDate(date) {
		return moment(date).format('DD/MM/YYYY');
	}
}