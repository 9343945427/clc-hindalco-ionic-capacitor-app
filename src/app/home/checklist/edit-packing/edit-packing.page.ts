import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { LoadingService } from 'src/app/services/login.service';
import { Storage } from '@ionic/storage';
@Component({
	selector: 'app-edit-packing',
	templateUrl: './edit-packing.page.html',
	styleUrls: ['./edit-packing.page.scss'],
})
export class EditPackingPage implements OnInit {
	token: any;
	packing_types:any
	meta = {
		packing_id: '',
		slip_id:'',
		trip_id:''
	}
	constructor(
		private modalCtrl: ModalController,
		private api: ApiService,
		private loading: LoadingService,
		private storage: Storage,
		private navParams: NavParams,
	) { 
		this.meta.slip_id = this.navParams.get('slip').slip_id
		this.meta.trip_id = this.navParams.get('slip').trip_id
		this.meta.packing_id =  this.navParams.get('slip').packing?.packing_id
	}

	async ngOnInit() {
		await this.storage.get('token').then((data) => {
			if (data != null) {
				this.token = data
				this.getPackingType()
			}
		});
	}
	async getPackingType() {
		await this.api.httpCall('getPackings', '', this.token).then(res => {
			this.packing_types = res['data']
		}, err => {
			console.log(err)
		})
	}

	async updatePackingTypes() {
		let payload = {
			packing_id: this.meta.packing_id,
			slip_id:this.meta.slip_id,
			trip_id : this.meta.trip_id
		}
		await this.loading.open()
		await this.api.httpCall('updateSlipPacking', payload, this.token).then(res => {
			this.loading.close()
			this.loading.message('Packing updated successfully').then(() => {
				this.modalCtrl.dismiss('update')
			})
		}, err => {
			this.loading.close()
			console.log(err)
		})
	}

	close() {
		this.modalCtrl.dismiss()
	}
}