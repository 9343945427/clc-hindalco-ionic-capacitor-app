import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditPackingPageRoutingModule } from './edit-packing-routing.module';

import { EditPackingPage } from './edit-packing.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditPackingPageRoutingModule
  ],
  declarations: [EditPackingPage]
})
export class EditPackingPageModule {}
