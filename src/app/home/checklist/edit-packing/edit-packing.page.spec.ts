import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EditPackingPage } from './edit-packing.page';

describe('EditPackingPage', () => {
  let component: EditPackingPage;
  let fixture: ComponentFixture<EditPackingPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(EditPackingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
