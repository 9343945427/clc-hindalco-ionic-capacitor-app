import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditPackingPage } from './edit-packing.page';

const routes: Routes = [
  {
    path: '',
    component: EditPackingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditPackingPageRoutingModule {}
