import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { LoadingService } from 'src/app/services/login.service';
import { Storage } from '@ionic/storage';
@Component({
	selector: 'app-edit-desiccant',
	templateUrl: './edit-desiccant.page.html',
	styleUrls: ['./edit-desiccant.page.scss'],
})
export class EditDesiccantPage implements OnInit {
	token: any;
	desiccants:any
	meta = {
		desiccant_id: '',
		slip_id:'',
		trip_id:''
	}
	constructor(
		private modalCtrl: ModalController,
		private api: ApiService,
		private loading: LoadingService,
		private storage: Storage,
		private navParams: NavParams,
	) { 
		this.meta.slip_id = this.navParams.get('slip').slip_id
		this.meta.trip_id = this.navParams.get('slip').trip_id
		this.meta.desiccant_id = this.navParams.get('slip').desiccant?.desiccant_id
	}

	async ngOnInit() {
		await this.storage.get('token').then((data) => {
			if (data != null) {
				this.token = data
				this. getDessicants()
			}
		});
	}

	async getDessicants() {
		await this.api.httpCall('getDesiccants', '', this.token).then(res => {
			this.desiccants = res['data']
		}, err => {
			console.log(err)
		})
	}

	async updateDesiccantTypes() {
		let payload = {
			desiccant_id: this.meta.desiccant_id,
			slip_id:this.meta.slip_id,
			trip_id : this.meta.trip_id
		}
		await this.loading.open()
		await this.api.httpCall('updateSlipDesiccant', payload, this.token).then(res => {
			this.loading.close()
			this.loading.message('Desiccant updated successfully').then(() => {
				this.modalCtrl.dismiss('update')
			})
		}, err => {
			this.loading.close()
			console.log(err)
		})
	}

	close() {
		this.modalCtrl.dismiss()
	}

}
