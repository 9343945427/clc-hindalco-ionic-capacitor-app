import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EditDesiccantPage } from './edit-desiccant.page';

describe('EditDesiccantPage', () => {
  let component: EditDesiccantPage;
  let fixture: ComponentFixture<EditDesiccantPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(EditDesiccantPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
