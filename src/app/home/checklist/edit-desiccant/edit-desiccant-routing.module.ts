import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditDesiccantPage } from './edit-desiccant.page';

const routes: Routes = [
  {
    path: '',
    component: EditDesiccantPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditDesiccantPageRoutingModule {}
