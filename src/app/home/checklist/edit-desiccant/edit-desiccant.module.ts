import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditDesiccantPageRoutingModule } from './edit-desiccant-routing.module';

import { EditDesiccantPage } from './edit-desiccant.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditDesiccantPageRoutingModule
  ],
  declarations: [EditDesiccantPage]
})
export class EditDesiccantPageModule {}
