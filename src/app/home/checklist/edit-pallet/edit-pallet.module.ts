import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditPalletPageRoutingModule } from './edit-pallet-routing.module';

import { EditPalletPage } from './edit-pallet.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditPalletPageRoutingModule
  ],
  declarations: [EditPalletPage]
})
export class EditPalletPageModule {}
