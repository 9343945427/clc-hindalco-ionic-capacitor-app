import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { LoadingService } from 'src/app/services/login.service';
import { Storage } from '@ionic/storage';
@Component({
	selector: 'app-edit-pallet',
	templateUrl: './edit-pallet.page.html',
	styleUrls: ['./edit-pallet.page.scss'],
})
export class EditPalletPage implements OnInit {
	token: any;
	pallets: any
	meta = {
		pallet_size_id: '',
		trip_id: '',
		slip_id: ''
	}
	constructor(
		private modalCtrl: ModalController,
		private api: ApiService,
		private loading: LoadingService,
		private storage: Storage,
		private navParams: NavParams,
	) {
		this.meta.slip_id = this.navParams.get('slip').slip_id
		this.meta.trip_id = this.navParams.get('slip').trip_id
		this.meta.pallet_size_id = this.navParams.get('slip').pallet_size?.pallet_size_id
	}

	async ngOnInit() {
		await this.storage.get('token').then((data) => {
			if (data != null) {
				this.token = data
				this.getPallets();
			}
		});
	}

	async getPallets() {
		await this.api.httpCall('getPalletSizes', '', this.token).then(res => {
			this.pallets = res['data']
		}, err => {
			console.log(err)
		})
	}

	async updatePallets() {
		let payload = {
			pallet_size_id: this.meta.pallet_size_id,
			slip_id: this.meta.slip_id,
			trip_id: this.meta.trip_id
		}
		await this.loading.open()
		await this.api.httpCall('updateSlipPalletSize', payload, this.token).then(res => {
			this.loading.close()
			this.loading.message('Pallet size updated successfully').then(() => {
				this.modalCtrl.dismiss('update')
			})
		}, err => {
			this.loading.close()
			console.log(err)
		})
	}

	close() {
		this.modalCtrl.dismiss()
	}

}
