import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditPalletPage } from './edit-pallet.page';

const routes: Routes = [
  {
    path: '',
    component: EditPalletPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditPalletPageRoutingModule {}
