import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EditPalletPage } from './edit-pallet.page';

describe('EditPalletPage', () => {
  let component: EditPalletPage;
  let fixture: ComponentFixture<EditPalletPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(EditPalletPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
