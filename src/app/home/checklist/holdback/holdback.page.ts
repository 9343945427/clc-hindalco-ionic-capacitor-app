import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { LoadingService } from 'src/app/services/login.service';
import { Storage } from '@ionic/storage';
@Component({
	selector: 'app-holdback',
	templateUrl: './holdback.page.html',
	styleUrls: ['./holdback.page.scss'],
})
export class HoldbackPage implements OnInit {
	token: any;
	reasons: any
	meta = {
		reason_id: '',
		trip_id: ''
	}
	constructor(
		private modalCtrl: ModalController,
		private api: ApiService,
		private loading: LoadingService,
		private storage: Storage,
		private navParams: NavParams,
	) {
		this.meta.trip_id = this.navParams.get('trip_id')
	}

	async ngOnInit() {
		await this.storage.get('token').then((data) => {
			if (data != null) {
				this.token = data
				this.getReasons()
			}
		});
		document.addEventListener('ionBackButton', (ev) => {
			console.log(ev)
		});
	}

	async getReasons() {
		await this.api.httpCall('getReasons', '', this.token).then(res => {
			this.reasons = res['data']
		}, err => {
			console.log(err)
		})
	}

	async updateReasons() {
		let payload = {
			reason_id: this.meta.reason_id,
			trip_id: this.meta.trip_id
		}
		await this.loading.open()
		await this.api.httpCall('updateTripReason', payload, this.token).then(res => {
			this.loading.close()
			this.loading.message('Trip Holdback reason updated successfully').then(() => {
				this.modalCtrl.dismiss('getTrip')
			})
		}, err => {
			this.loading.close()
			console.log(err)
		})
	}

}