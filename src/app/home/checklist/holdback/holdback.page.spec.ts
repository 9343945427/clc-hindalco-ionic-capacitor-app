import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HoldbackPage } from './holdback.page';

describe('HoldbackPage', () => {
  let component: HoldbackPage;
  let fixture: ComponentFixture<HoldbackPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(HoldbackPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
