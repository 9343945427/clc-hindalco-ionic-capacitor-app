import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HoldbackPageRoutingModule } from './holdback-routing.module';

import { HoldbackPage } from './holdback.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HoldbackPageRoutingModule
  ],
  declarations: [HoldbackPage]
})
export class HoldbackPageModule {}
