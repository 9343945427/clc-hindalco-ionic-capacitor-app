import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HoldbackPage } from './holdback.page';

const routes: Routes = [
  {
    path: '',
    component: HoldbackPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HoldbackPageRoutingModule {}
