import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Camera, CameraDirection, CameraResultType, CameraSource } from '@capacitor/camera';
import { MediaCapture, MediaFile, CaptureError, CaptureVideoOptions } from '@awesome-cordova-plugins/media-capture/ngx';
import { ActionSheetController, AlertController, ModalController, Platform } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { LoadingService } from 'src/app/services/login.service';
import { EditPage } from './edit/edit.page';
import { CreatePage } from './create/create.page';
import { Filesystem } from '@capacitor/filesystem';
import * as moment from 'moment';
import { HoldbackPage } from './holdback/holdback.page';
import { EditLoadingTypePage } from './edit-loading-type/edit-loading-type.page';
import { EditPackingPage } from './edit-packing/edit-packing.page';
import { EditPalletPage } from './edit-pallet/edit-pallet.page';
import { EditDesiccantPage } from './edit-desiccant/edit-desiccant.page';

@Component({
	selector: 'app-checklist',
	templateUrl: './checklist.page.html',
	styleUrls: ['./checklist.page.scss'],
})
export class ChecklistPage implements OnInit {
	segment = 'info'
	videos = []
	trip: any;
	user: any;
	token: any;
	slips: any;
	loading_types: any;
	trip_stages: any;
	selected: any = 0;
	trip_checks: any;
	stage_name = ''
	trip_attachments: any
	valid_container_no: any;
	valid_container_no_message = ''
	show_container_no: any;
	pending_stages = []
	current_index: any
	trip_check_attachments: any
	constructor(
		private api: ApiService,
		private loading: LoadingService,
		private storage: Storage,
		private mediaCapture: MediaCapture,
		private actionSheetController: ActionSheetController,
		public router: Router,
		private route: ActivatedRoute,
		private modalCtrl: ModalController,
		public alertController: AlertController,
		private platform: Platform,
	) {
		this.route.queryParams.subscribe(params => {
			if (this.router.getCurrentNavigation().extras.state) {
				this.trip = this.router.getCurrentNavigation().extras.state['trip'];
			}
		});
		this.platform.backButton.subscribeWithPriority(10, (processNextHandler) => {
			this.confirmSavedData()
		});
	}

	async ngOnInit() {
	}

	async ionViewWillEnter() {
		await this.storage.get('user').then((data) => {
			if (data != null) {
				this.user = data;
			}
		});
		await this.storage.get('token').then((data) => {
			if (data != null) {
				this.token = data;
				this.checkTripDate()
			}
		});
		this.api.can_leave = false
		this.segment == 'info'
	}

	async ionViewWillLeave() {

	}

	checkTripDate() {
		let date1 = new Date(moment(this.trip.trip_date_time).format('MM/DD/YYYY'));
		let date2 = new Date(moment().format('MM/DD/YYYY'));
		// Calculating the time difference
		// of two dates
		let Difference_In_Time =
			date2.getTime() - date1.getTime();
		// Calculating the no. of days between
		// two dates
		let Difference_In_Days =
			Math.round
				(Difference_In_Time / (1000 * 3600 * 24));
		if (Difference_In_Days >= 1) {
			this.goToHoldback()
		} else {
			this.getTrip()
		}
	}

	async goToHoldback() {
		const modal = await this.modalCtrl.create({
			component: HoldbackPage,
			componentProps: {
				trip_id: this.trip.trip_id
			}
		});
		modal.present();
		modal.onDidDismiss().then((data) => {
			if (data.data == 'getTrip') {
				this.getTrip();
			}
			else {
				alert('Please provide reason for holdback')
				this.goToHoldback()
			}
		})
	}

	async getTrip() {
		let payload = {
			trip_id: this.trip.trip_id,
		}
		await this.loading.open()
		await this.api.httpCall('getTrip', payload, this.token).then(res => {
			this.loading.close();
			this.slips = res['data'].slips
			if (res['data'].trip_stages.length == 0) {
				this.trip_stages = []
				this.getLoadingTypes()
			}
			else {
				this.trip_stages = res['data'].trip_stages
			}
			this.pending_stages = this.trip_stages.filter(pending_stage => {
				return (pending_stage.status == 0 || pending_stage.status == null)
			})
			if (res['data'].container_no == null || res['data'].container_no == '') {
				this.show_container_no = true
			}
			else {
				this.show_container_no = false
			}
		}, err => {
			this.loading.close()
			console.log(err)
		})
	}

	async getLoadingTypes() {
		await this.loading.open()
		await this.api.httpCall('getLoadingTypes', '', this.token).then(res => {
			this.loading.close();
			this.loading_types = res['data']
		}, err => {
			this.loading.close()
			console.log(err)
		})
	}

	async selectLoadingType(e) {
		let payload = {
			loading_type_id: e.detail.value,
			trip_id: this.trip.trip_id
		}
		await this.loading.open()
		await this.api.httpCall('generateTripStages', payload, this.token).then(res => {
			this.loading.close();
			this.loading_types = !this.loading_types
			this.trip_stages = res['data']
		}, err => {
			this.loading.close()
			console.log(err)
		})
	}

	async checkSegment(e) {
		if (this.trip.container_no !== null || this.trip.container_no !== '') {
			this.segment == 'info' ? this.stage_name = this.segment : 0

		} if (this.trip.container_no == '' || this.trip.container_no == null) {
			setTimeout(() => {
				this.segment = 'info'
				this.stage_name = this.segment
			}, 200);
		}
	}

	async getPendingTripChecks(trip_stage, i?) {
		if (this.trip.container_no == '' || this.trip.container_no == null) {
			alert('Please update Container number before continuing.');
		}
		else {
			let current_stage = trip_stage
			let previous_stage = this.pending_stages.filter(stage => {
				return this.stage_name == stage.stage_name
			})
			if (previous_stage.length != 0) {
				if (current_stage.status == previous_stage[0].status) {
					if (current_stage.status == 0 && previous_stage[0].status == 0 && previous_stage[0].draft != 1) {
						alert('Please submit current stage')
						setTimeout(() => {
							this.segment = previous_stage[0].stage_name
							this.stage_name = this.segment
						}, 200);
					}
					else {
						console.log('179')
						let payload = {
							trip_stage_id: trip_stage.trip_stage_id,
						}
						this.segment = this.trip_stages.indexOf(trip_stage)
						this.stage_name = trip_stage.stage_name
						let stage = this.trip_stages.filter(stage => {
							return stage.stage_name == this.stage_name
						})
						this.current_index = Math.abs(this.trip_stages.indexOf(stage[0]))
						await this.loading.open()
						await this.api.httpCall('getTripChecks', payload, this.token).then(res => {
							this.loading.close();
							this.trip_checks = res['data'];
							this.trip_attachments = res['trip_attachments']
							this.trip_check_attachments = this.trip_checks.filter(ele => {

								if (ele.trip_attachments.length != 0) return ele
								else return null
							})

						}, err => {
							this.loading.close()
							console.log(err)
						})
					}
				}
				else if (this.pending_stages.length != 0 && i == this.trip_stages.length - 1) {
					alert('Please complete all stages')
					setTimeout(() => {
						this.segment = 'info'
						this.stage_name = ''
					}, 200);
				}
				else if (current_stage.status == 1 && previous_stage[0].status == 0 && previous_stage[0].draft != 1) {
					alert('Please submit current stage')
					setTimeout(() => {
						this.segment = previous_stage[0].stage_name
						this.stage_name = this.segment
					}, 200);
				}
				else if (current_stage.status == 0 && previous_stage[0].status == 1 && previous_stage[0].draft != 1) {
					alert('Please submit current stage')
					setTimeout(() => {
						this.segment = previous_stage[0].stage_name
						this.stage_name = this.segment
					}, 200);
				}
				else {
					console.log('223')
					let payload = {
						trip_stage_id: trip_stage.trip_stage_id,
					}
					this.segment = this.trip_stages.indexOf(trip_stage)
					this.stage_name = trip_stage.stage_name
					await this.loading.open()
					await this.api.httpCall('getTripChecks', payload, this.token).then(res => {
						this.loading.close();
						this.trip_checks = res['data'];
						this.trip_attachments = res['trip_attachments']
						this.trip_check_attachments = this.trip_checks.filter(ele => {

							if (ele.trip_attachments.length != 0) return ele
							else return null
						})
						let stage = this.trip_stages.filter(stage => {
							return stage.stage_name == this.stage_name
						})
						this.current_index = Math.abs(this.trip_stages.indexOf(stage[0]))
						console.log(this.current_index)
					}, err => {
						this.loading.close()
						console.log(err)
					})
				}
			} else {
				if (this.pending_stages.length != 0 && i == this.trip_stages.length - 1) {
					if (current_stage.trip_stage_id == this.pending_stages[0].trip_stage_id) {
						console.log(247)
						let payload = {
							trip_stage_id: trip_stage.trip_stage_id,
						}
						this.segment = this.trip_stages.indexOf(trip_stage)
						this.stage_name = trip_stage.stage_name
						let stage = this.trip_stages.filter(stage => {
							return stage.stage_name == this.stage_name
						})
						this.current_index = Math.abs(this.trip_stages.indexOf(stage[0]))
						console.log(this.current_index)
						await this.loading.open()
						await this.api.httpCall('getTripChecks', payload, this.token).then(res => {
							this.loading.close();
							this.trip_checks = res['data'];
							this.trip_attachments = res['trip_attachments']
							this.trip_check_attachments = this.trip_checks.filter(ele => {

								if (ele.trip_attachments.length != 0) return ele
								else return null
							})

						}, err => {
							this.loading.close()
							console.log(err)
						})
					}
					else {
						var drafted_stages = []
						console.log(current_stage)
						drafted_stages = this.pending_stages.filter(stage => {
							return stage.draft == 0 && current_stage.trip_stage != stage.trip_stage_id
						})
						if (drafted_stages[0].trip_stage_id == current_stage.trip_stage_id) drafted_stages = []
						if (drafted_stages.length == 0) {
							let payload = {
								trip_stage_id: trip_stage.trip_stage_id,
							}
							this.segment = this.trip_stages.indexOf(trip_stage)
							this.stage_name = trip_stage.stage_name
							await this.loading.open()
							await this.api.httpCall('getTripChecks', payload, this.token).then(res => {
								this.loading.close();
								this.trip_checks = res['data'];
								this.trip_attachments = res['trip_attachments']
								this.trip_check_attachments = this.trip_checks.filter(ele => {

									if (ele.trip_attachments.length != 0) return ele
									else return null
								})
								let stage = this.trip_stages.filter(stage => {
									return stage.stage_name == this.stage_name
								})
								this.current_index = Math.abs(this.trip_stages.indexOf(stage[0]))
							}, err => {
								this.loading.close()
								console.log(err)
							})
						} else {
							alert('Please complete all stages')
							setTimeout(() => {
								this.segment = 'info'
								this.stage_name = ''
							}, 200);
						}
					}
				} else {
					console.log('303')
					let payload = {
						trip_stage_id: trip_stage.trip_stage_id,
					}
					this.segment = this.trip_stages.indexOf(trip_stage)
					this.stage_name = trip_stage.stage_name
					await this.loading.open()
					await this.api.httpCall('getTripChecks', payload, this.token).then(res => {
						this.loading.close();
						this.trip_checks = res['data'];
						this.trip_attachments = res['trip_attachments']
						this.trip_check_attachments = this.trip_checks.filter(ele => {

							if (ele.trip_attachments.length != 0) return ele
							else return null
						})
						let stage = this.trip_stages.filter(stage => {
							return stage.stage_name == this.stage_name
						})
						this.current_index = Math.abs(this.trip_stages.indexOf(stage[0]))
					}, err => {
						this.loading.close()
						console.log(err)
					})
				}

			}
		}

	}

	async getTripChecks(trip_stage, i?) {
		let payload = {
			trip_stage_id: trip_stage.trip_stage_id,
		}
		this.segment = this.trip_stages.indexOf(trip_stage)
		this.stage_name = trip_stage.stage_name
		await this.loading.open()
		await this.api.httpCall('getTripChecks', payload, this.token).then(res => {
			this.loading.close();
			this.trip_checks = res['data'];
			this.trip_attachments = res['trip_attachments']
			this.trip_check_attachments = this.trip_checks.filter(ele => {

				if (ele.trip_attachments.length != 0) return ele
				else return null
			})
		}, err => {
			this.loading.close()
			console.log(err)
		})
	}

	async editPacking(slip) {
		const modal = await this.modalCtrl.create({
			component: EditPackingPage,
			componentProps: {
				slip: slip
			}
		});
		modal.present();
		modal.onDidDismiss().then((data) => {
			if (data.data == 'update') {
				this.getTrip();
			}
		})
	}

	async editLoadingType(slip) {
		const modal = await this.modalCtrl.create({
			component: EditLoadingTypePage,
			componentProps: {
				slip: slip
			}
		});
		modal.present();
		modal.onDidDismiss().then((data) => {
			if (data.data == 'update') {
				this.getTrip();
			}
		})
	}

	async editPallet(slip) {
		const modal = await this.modalCtrl.create({
			component: EditPalletPage,
			componentProps: {
				slip: slip
			}
		});
		modal.present();
		modal.onDidDismiss().then((data) => {
			if (data.data == 'update') {
				this.getTrip();
			}
		})
	}

	async editDessicant(slip) {
		const modal = await this.modalCtrl.create({
			component: EditDesiccantPage,
			componentProps: {
				slip: slip
			}
		});
		modal.present();
		modal.onDidDismiss().then((data) => {
			if (data.data == 'update') {
				this.getTrip();
			}
		})
	}

	checkContainerNo(e, trip) {
		trip.container_no = trip.container_no.replace(/\s/g, "").toUpperCase();
		let pattern = /[-’/`~!#*$₹@_%+=.,"^&(){}[\]|;:”<>?\\]/
		if (pattern.test(trip.container_no) == true) {
			trip.container_no = trip.container_no.replace(/[-’/`~!#*$@_%+=.,"^&(){}[\]|;:”<>?\\]/g, '').toUpperCase()
			this.valid_container_no = false
			this.valid_container_no_message = 'Container number cannot contain special characters ~`! @#$%^&*()-_+={}[]|\;:"<>,./?'
		} else {
			this.valid_container_no_message = ''
		}
	}

	//for updating Container No
	async updateTrip(trip) {
		trip.container_no = trip.container_no.replace(/\s/g, "");
		trip.container_no = trip.container_no.replace(/[-’/`~!#*$@_%+=.,"^&(){}[\]|;:”<>?\\]/g, '').toUpperCase()
		let payload = {
			trip_id: trip.trip_id,
			trip_no: trip.trip_no,
			trip_date_time: trip.trip_date_time,
			transporter_id: trip.transporter.transporter_id,
			wagon_no: trip.wagon_no,
			container_no: trip.container_no.replace(/^\s+|\s+$/gm, ''),
			note: trip.note,
			customer_id: trip.customer.customer_id,
			destination: trip.destination,
			slips: trip.slips
		}
		await this.loading.open()
		await this.api.httpCall('updateTripContainerNo', payload, this.token).then(res => {
			this.loading.close();
			this.loading.message('Container number Updated successfully.')
			this.getTrip()
		}, err => {
			this.loading.close()
			console.log(err)
		})
	}

	async selectLotOption(lot) {
		const actionSheet = await this.actionSheetController.create({
			header: 'Select one option',
			buttons: [
				{
					text: 'Edit',
					icon: 'pencil-outline',
					handler: async () => {
						this.editLot(lot)
					}
				},
				{
					text: 'Delete',
					icon: 'trash-outline',
					handler: () => {
						this.confirmDelete(lot.lot_id)
					}
				},
				{
					text: 'Cancel',
					icon: 'close-circle-outline',
					role: 'cancel'
				}
			]
		});
		await actionSheet.present();
	}

	async createLot(slip) {
		let slip_details = {
			user_id: this.user.user_id,
			plant_id: this.user.plant.plant_id,
			trip_id: slip.trip_id,
			slip_id: slip.slip_id,
		}
		const modal = await this.modalCtrl.create({
			component: CreatePage,
			componentProps: slip_details
		});
		modal.present();
		modal.onDidDismiss().then(data => {
			if (data.data == 'update') {
				this.getTrip();
			}
		})
	}

	async editLot(lot) {
		let lot_details = {
			lot: lot,
			user_id: this.user.user_id,
			plant_id: this.user.plant.plant_id,
			trip_id: lot.trip_id,
			slip_id: lot.slip_id,
		}
		const modal = await this.modalCtrl.create({
			component: EditPage,
			componentProps: lot_details
		});
		modal.present();
		modal.onDidDismiss().then(data => {
			if (data.data == 'update') {
				this.getTrip()
			}
		})
	}

	async deleteLot(lot_id) {
		let payload = {
			lot_id: lot_id
		}
		await this.loading.open()
		await this.api.httpCall('deleteLot', payload, this.token).then(res => {
			this.loading.close();
			this.loading.message('Lot deleted successfully.')
			this.getTrip()
		}, err => {
			this.loading.close()
			console.log(err)
		})
	}

	async confirmDelete(lot_id) {
		const alert = await this.alertController.create({
			header: 'Confirm !',
			cssClass: "confirm-alert",
			message: 'Are you sure ?',
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel',
					id: 'cancel-button',
					handler: () => {
						return false
					}
				}, {
					text: 'Delete',
					id: 'confirm-button',
					handler: () => {
						this.deleteLot(lot_id)
					}
				}
			]
		});
		await alert.present();
	}

	async confirmSubmitChecks(trip_checks, trip_stages, draft) {
		let empty_fields = []
		for (let trip_check of trip_checks) {
			if (trip_check.value == '' || trip_check.value == null || !trip_check.value) {
				empty_fields.push(trip_check)
			}
		}
		if (empty_fields.length !== 0) {
			this.loading.presentAlert('Alert', 'All fields are mandatory. Please fill all fields in order to submit.')
		}
		else {
			const alert = await this.alertController.create({
				header: 'Confirm !',
				message: 'Please make sure all checks are answered and also Images are taken before continuing',
				cssClass: "confirm-alert",
				buttons: [
					{
						text: 'Cancel',
						role: 'cancel',
						id: 'cancel-button',
						handler: () => {
							return false
						}
					}, {
						text: 'Continue',
						id: 'confirm-button',
						handler: () => {
							this.submitChecks(trip_checks, trip_stages, draft)
						}
					}
				]
			});
			await alert.present();
		}
	}

	async confirmDraftStage(trip_checks, trip_stages, draft) {
		this.submitChecks(trip_checks, trip_stages, draft)
	}

	async confirmSubmitTrip(trip_checks, trip_stages, draft) {
		let empty_fields = []
		for (let trip_check of trip_checks) {
			if (trip_check.value == '' || trip_check.value == null || !trip_check.value) {
				empty_fields.push(trip_check)
			}
		}
		if (empty_fields.length !== 0) {
			this.loading.presentAlert('Alert', 'All fields are mandatory. Please fill all fields in order to submit.')
		}
		else {
			const alert = await this.alertController.create({
				header: 'Confirm !',
				cssClass: 'confirm-alert',
				message: 'You are about to submit last stage of the trip. Please make sure all stages are submitted and all checks are answered. Any unsaved data will be lost!',
				buttons: [
					{
						text: 'Cancel',
						role: 'cancel',
						id: 'cancel-button',
						handler: () => {
							return false
						}
					}, {
						text: 'Continue',
						id: 'confirm-button',
						handler: () => {
							this.submitChecks(trip_checks, trip_stages, 0)
						}
					}
				]
			});
			await alert.present();
		}
	}

	async submitChecks(trip_checks, trip_stages, draft) {
		let payload = {
			trip_checks: trip_checks,
			plant_id: trip_checks[0]?.plant_id,
			trip_id: trip_checks[0]?.trip_id,
			trip_stage_id: trip_checks[0]?.trip_stage_id,
			attachments: trip_checks['trip_attachments'],
			note: null,
			draft: draft
		}
		let note = trip_stages.filter((trip_stage) => {
			return (trip_stage.trip_stage_id == payload.trip_stage_id)
		})
		payload.note = note[0]?.note
		let empty_note = []
		for (let trip_check of payload.trip_checks) {
			let empty_value = trip_check.value.toString().toLowerCase()
			if (empty_value == 'no' && payload.note == null || payload.note == '') {
				empty_note.push(trip_check)
			}
		}
		if (empty_note.length !== 0) {
			this.loading.presentAlert('Alert', 'We have discovered some deviated checks. Please provide additional comments to continue..')
		} else {
			await this.loading.open()
			await this.api.httpCall('updateTripCheck', payload, this.token).then(async res => {
				this.loading.close();
				this.pending_stages = res['pending_trip_stages']
				this.loading.message('Checks updated successfully.')
				let index: any
				index = trip_stages.map(e => e.trip_stage_id).indexOf(payload.trip_stage_id);
				if (index == trip_stages.length - 1) {
					// this.segment = 'info'
					// this.stage_name = ''
					this.router.navigate(['/tabs/home'])

				}
				else {
					let trip_stage = trip_stages[index + 1]
					this.segment = trip_stage.stage_name
					this.stage_name = trip_stage.stage_name
					this.current_index = Math.abs(this.trip_stages.indexOf(trip_stage))
					this.getTripChecks(trip_stage)
				}
			}, err => {
				this.loading.close();
				console.log(err)
			})

		}

	}

	async capturePhotos(trip_checks) {
		if (trip_checks.trip_attachments) {
			let max_images = []
			max_images = trip_checks.trip_attachments.filter(img => {
				return img.type == 'image'
			})
			if (max_images.length == 15) {
				alert('Max 15 Images allowed')
			}
			else {
				const image = await Camera.getPhoto({
					quality: 50,
					allowEditing: false,
					resultType: CameraResultType.Base64,
					source: CameraSource.Camera,
					direction: CameraDirection.Rear
				});
				let imageUrl = 'data:image/jpeg;base64,' + image.base64String;
				let attachment_id = Math.floor(new Date().getTime())
				let attachment = {
					attachment: imageUrl,
					type: 'image',
					attachment_id: attachment_id
				}
				if (!trip_checks.trip_attachments) {
					trip_checks.trip_attachments = [];
				}
				trip_checks.trip_attachments.push(attachment)
			}
		} else {
			const image = await Camera.getPhoto({
				quality: 50,
				allowEditing: false,
				resultType: CameraResultType.Base64,
				source: CameraSource.Camera
			});
			let imageUrl = 'data:image/jpeg;base64,' + image.base64String;
			let attachment_id = Math.floor(new Date().getTime())
			let attachment = {
				attachment: imageUrl,
				type: 'image',
				attachment_id: attachment_id
			}
			if (!trip_checks.trip_attachments) {
				trip_checks.trip_attachments = [];
			}
			trip_checks.trip_attachments.push(attachment)
		}
	}

	async recordVideo(trip_checks) {
		let vm = this
		let options: CaptureVideoOptions = {
			limit: 1,
			duration: 10,
			quality: 10
		}
		vm.mediaCapture.captureVideo(options).then(async (res: MediaFile[]) => {
			const contents = await Filesystem.readFile({
				path: res[0].fullPath,
			});
			let attachment = {
				attachment: 'data:video/mp4;base64,' + contents.data,
				type: 'video',
				attachment_id: Math.floor(new Date().getTime())
			}
			if (!trip_checks.trip_attachments) {
				trip_checks.trip_attachments = [];
			}
			trip_checks.trip_attachments.push(attachment)
		}, (err: CaptureError) => console.error(err));
	}

	deleteAttachment(trip_checks, attachment_id) {
		trip_checks.trip_attachments = trip_checks.trip_attachments.filter(attachment => {
			if (!attachment.check_id) {
				return attachment.attachment_id !== attachment_id
			}
			else {
				let trip_check = trip_checks.filter(ele => {
					return ele.trip_check_id == attachment.check_id
				})
				console.log(trip_check)
				trip_check[0].value = ''
				return attachment.attachment_id !== attachment_id
			}

		});
	}

	async selectMedia(trip_checks) {
		const actionSheet = await this.actionSheetController.create({
			header: 'What would you like to add?',
			buttons: [
				{
					text: 'Capture Image',
					icon: 'camera-outline',
					handler: () => {
						this.capturePhotos(trip_checks);
					}
				},
				{
					text: 'Capture Video',
					icon: 'videocam-outline',
					handler: () => {
						this.recordVideo(trip_checks);
					}
				},
				{
					text: 'Cancel',
					icon: 'close-circle-outline',
					role: 'cancel'
				}
			]
		});
		await actionSheet.present();
	}

	checkColor(trip_check) {
		if (trip_check.min == '' || trip_check.max == '' || !trip_check.min || !trip_check.max || trip_check.value == '' || trip_check.value == '') {
			return false;
		}
		if (trip_check.value < trip_check.min || trip_check.value > trip_check.max) {
			return true;
		}
		return false;
	}

	async handleChange(e, trip_check, trip_checks) {
		let value = ''
		if (!trip_checks.trip_attachments) {
			trip_checks.trip_attachments = [];
		}
		if (!trip_check.attachments) {
			trip_check.attachments = [];
		}
		if (trip_check.is_image_required == 1) {
			value = e.detail.value.toLowerCase()
			if (value == 'yes') {
				await this.platform.ready().then(async res => {
					const image = await Camera.getPhoto({
						quality: 50,
						allowEditing: false,
						resultType: CameraResultType.Base64,
						source: CameraSource.Camera,
						direction: CameraDirection.Rear
					}).then(async res => {
						let imageUrl = 'data:image/jpeg;base64,' + res.base64String;
						let attachment_id = Math.floor(new Date().getTime())
						let attachment = {
							attachment: imageUrl,
							type: 'image',
							attachment_id: attachment_id,
							check_id: trip_check.trip_check_id
						}
						if (!trip_checks.trip_attachments) {
							trip_checks.trip_attachments = [];
						}
						if (!trip_check.attachments) {
							trip_check.attachments = [];
						}
						trip_checks.trip_attachments.push(attachment)
						trip_check.attachments.push(attachment)
						trip_check.value = e.detail.value
					}, err => {
						console.log(err)
						alert('Image is compulsory for this check')
						trip_check.value = ''
					});
				})
			} else {
				this.confirmTripCheckValue(trip_check, e.detail.value, trip_checks)
			}
		}
		else {
			if (!trip_check.attachments) {
				trip_check.attachments = [];
			}
			value = e.detail.value.toLowerCase()
			if (value == 'yes') {
				trip_check.value = e.detail.value
			} else {
				this.confirmTripCheckValue(trip_check, e.detail.value, trip_checks)
			}

		}
	}

	async checkImageRequired(trip_check, trip_checks) {
		if (trip_check.is_image_required == 1) {
			await this.platform.ready().then(async res => {
				const image = await Camera.getPhoto({
					quality: 50,
					allowEditing: false,
					resultType: CameraResultType.Base64,
					source: CameraSource.Camera,
					direction: CameraDirection.Rear
				}).then(async res => {
					let imageUrl = 'data:image/jpeg;base64,' + res.base64String;
					let attachment_id = Math.floor(new Date().getTime())
					let attachment = {
						attachment: imageUrl,
						type: 'image',
						attachment_id: attachment_id,
						check_id: trip_check.trip_check_id
					}
					if (!trip_checks.trip_attachments) {
						trip_checks.trip_attachments = [];
					}
					if (!trip_check.attachments) {
						trip_check.attachments = [];
					}
					trip_checks.trip_attachments.push(attachment)
					trip_check.attachments.push(attachment)
				}, err => {
					console.log(err)
					alert('Image is compulsory for this check')
					trip_check.value = ''
				});
			})
		} else {
			if (!trip_check.attachments) {
				trip_check.attachments = [];
			}
		}
		// if (e.detail.value == 'YES' && trip_check.is_image_required == 1) {
		// 	await this.platform.ready().then(async res => {
		// 		const image = await Camera.getPhoto({
		// 			quality: 50,
		// 			allowEditing: false,
		// 			resultType: CameraResultType.Base64,
		// 			source: CameraSource.Camera,
		// 			direction: CameraDirection.Rear
		// 		}).then(async res => {
		// 			trip_check.value = e.detail.value
		// 			let imageUrl = 'data:image/jpeg;base64,' + res.base64String;
		// 			let attachment_id = Math.floor(new Date().getTime())
		// 			let attachment = {
		// 				attachment: imageUrl,
		// 				type: 'image',
		// 				attachment_id: attachment_id,
		// 				check_id: trip_check.trip_check_id
		// 			}
		// 			if (!trip_checks.trip_attachments) {
		// 				trip_checks.trip_attachments = [];
		// 			}
		// 			if (!trip_check.attachments) {
		// 				trip_check.attachments = [];
		// 			}
		// 			trip_checks.trip_attachments.push(attachment)
		// 			trip_check.attachments.push(attachment)
		// 		}, err => {
		// 			console.log(err)
		// 			alert('Image is compulsory for this check')
		// 			trip_check.value = ''
		// 		});
		// 	})
		// }
		// else if (e.detail.value == 'NO') {
		// 	this.confirmTripCheckValue(trip_check, e.detail.value)
		// 	if (!trip_check.attachments) {
		// 		trip_check.attachments = [];
		// 	}
		// }
		// else {
		// 	if (!trip_check.attachments) {
		// 		trip_check.attachments = [];
		// 	}
		// 	trip_check.value = e.detail.value
		// }
	}

	async confirmTripCheckValue(trip_check, value, trip_checks) {
		const alert = await this.alertController.create({
			header: 'Confirm !',
			cssClass: 'confirm-alert',
			message: 'Are you sure? If Yes, then please add Comments in the Additional Comments field at the bottom. ',
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel',
					id: 'cancel-button',
					handler: () => {
						trip_check.value = ''
					}
				}, {
					text: 'Continue',
					id: 'confirm-button',
					handler: async () => {
						await this.platform.ready().then(async res => {
							const image = await Camera.getPhoto({
								quality: 50,
								allowEditing: false,
								resultType: CameraResultType.Base64,
								source: CameraSource.Camera,
								direction: CameraDirection.Rear
							}).then(async res => {
								let imageUrl = 'data:image/jpeg;base64,' + res.base64String;
								let attachment_id = Math.floor(new Date().getTime())
								let attachment = {
									attachment: imageUrl,
									type: 'image',
									attachment_id: attachment_id,
									check_id: trip_check.trip_check_id
								}
								if (!trip_checks.trip_attachments) {
									trip_checks.trip_attachments = [];
								}
								if (!trip_check.attachments) {
									trip_check.attachments = [];
								}
								trip_checks.trip_attachments.push(attachment)
								trip_check.attachments.push(attachment)
								trip_check.value = value
							}, err => {
								console.log(err)
								this.loading.presentAlert('Alert', 'Image is compulsory for this check')
								trip_check.value = ''
							});
						})

					}
				}
			]
		});
		await alert.present();
	}

	convertDate(date) {
		return moment(date).format('DD/MM/YYYY');
	}

	async confirmSavedData() {
		this.router.navigate(['tabs/home'])
	}

	async confirmDiscardTrip() {
		//Confirming before discarding trip. 
		const alert = await this.alertController.create({
			header: 'Confirm !',
			cssClass: 'confirm-alert',
			message: 'You are about to take action on this trip. This action cannot be reversed!',
			buttons: [
				{
					text: 'Discard',
					id: 'confirm-button',
					handler: () => {
						this.discardTrip()
					}
				},
				{
					text: 'Terminate',
					id: 'confirm-button',
					handler: () => {
						this.terminateTrip()
					}
				},
				{
					text: 'Cancel',
					role: 'cancel',
					id: 'cancel-button',
					handler: () => {
						return;
					}
				},
			]
		});
		await alert.present();
	}

	async discardTrip() {
		let payload = {
			trip_id: this.trip.trip_id
		}
		await this.loading.open()
		await this.api.httpCall('discardTrip', payload, this.token).then(async res => {
			this.loading.close();
			this.api.can_leave = true
			this.router.navigate(['/tabs/home'])
		}, err => {
			this.loading.close();
			console.log(err)
		})
	}

	async terminateTrip() {
		let payload = {
			trip_id: this.trip.trip_id
		}
		await this.loading.open()
		await this.api.httpCall('terminateTrip', payload, this.token).then(async res => {
			this.loading.close();
			this.api.can_leave = true
			this.router.navigate(['/tabs/home'])
		}, err => {
			this.loading.close();
			console.log(err)
		})
	}
}