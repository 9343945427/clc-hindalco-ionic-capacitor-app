import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditLoadingTypePageRoutingModule } from './edit-loading-type-routing.module';

import { EditLoadingTypePage } from './edit-loading-type.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditLoadingTypePageRoutingModule
  ],
  declarations: [EditLoadingTypePage]
})
export class EditLoadingTypePageModule {}
