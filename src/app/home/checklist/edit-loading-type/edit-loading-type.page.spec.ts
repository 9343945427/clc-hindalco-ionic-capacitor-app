import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EditLoadingTypePage } from './edit-loading-type.page';

describe('EditLoadingTypePage', () => {
  let component: EditLoadingTypePage;
  let fixture: ComponentFixture<EditLoadingTypePage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(EditLoadingTypePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
