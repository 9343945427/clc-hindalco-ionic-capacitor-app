import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { LoadingService } from 'src/app/services/login.service';
import { Storage } from '@ionic/storage';
@Component({
	selector: 'app-edit-loading-type',
	templateUrl: './edit-loading-type.page.html',
	styleUrls: ['./edit-loading-type.page.scss'],
})
export class EditLoadingTypePage implements OnInit {
	token: any;
	loading_types:any
	meta = {
		loading_type_id: '',
		slip_id:'',
		trip_id:''
	}
	constructor(
		private modalCtrl: ModalController,
		private api: ApiService,
		private loading: LoadingService,
		private storage: Storage,
		private navParams: NavParams,
	) { 
		this.meta.slip_id = this.navParams.get('slip').slip_id
		this.meta.trip_id = this.navParams.get('slip').trip_id
		this.meta.loading_type_id = this.navParams.get('slip').loading_type?.loading_type_id
	}

	async ngOnInit() {
		await this.storage.get('token').then((data) => {
			if (data != null) {
				this.token = data
				this. getLoadingType()
			}
		});
	}
	async getLoadingType() {
		await this.api.httpCall('getLoadingTypes', '', this.token).then(res => {
			this.loading_types = res['data']
		}, err => {
			console.log(err)
		})
	}

	async updateloadingTypes() {
		let payload = {
			loading_type_id: this.meta.loading_type_id,
			slip_id:this.meta.slip_id,
			trip_id : this.meta.trip_id
		}
		await this.loading.open()
		await this.api.httpCall('updateSlipLoadingType', payload, this.token).then(res => {
			this.loading.close()
			this.loading.message('Loading Type updated successfully').then(() => {
				this.modalCtrl.dismiss('update')
			})
		}, err => {
			this.loading.close()
			console.log(err)
		})
	}

	close() {
		this.modalCtrl.dismiss()
	}

}
