import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditLoadingTypePage } from './edit-loading-type.page';

const routes: Routes = [
  {
    path: '',
    component: EditLoadingTypePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditLoadingTypePageRoutingModule {}
