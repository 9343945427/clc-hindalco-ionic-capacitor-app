import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ModalController, NavController, NavParams } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ApiService } from 'src/app/services/api.service';
import { LoadingService } from 'src/app/services/login.service';
@Component({
	selector: 'app-edit',
	templateUrl: './edit.page.html',
	styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {
	meta = {
		lot: null,
		plant_id: null,
		trip_id: null,
		slip_id: null,
		storage_id: null,
		lot_no: null,
		loading_bags: null,
		loading_quantity: null
	}
	user: any;
	token: any;
	storages: any;
	valid_lot_no: any;
	valid_lot_no_message = ''
	
	constructor(
		private storage: Storage,
		private api: ApiService,
		private loading: LoadingService,
		private route: ActivatedRoute,
		public router: Router,
		private navctrl: NavController,
		private navParams: NavParams,
		private modalCtrl: ModalController,
	) {
		this.meta.lot = this.navParams.data['lot'];
		this.meta.plant_id = this.navParams.data['plant_id'];
		this.meta.trip_id = this.navParams.data['trip_id'];
		this.meta.slip_id = this.navParams.data['slip_id'];
		this.meta.lot_no = this.navParams.data['lot'].lot_no;
		this.meta.loading_bags = this.navParams.data['lot'].loading_bags;
		this.meta.loading_quantity = this.navParams.data['lot'].loading_quantity;
		this.meta.storage_id = this.navParams.data['lot'].storage.storage_id;
	}

	async ngOnInit() {
		await this.storage.get('user').then((data) => {
			if (data != null) {
				this.user = data;
			}
		});
		await this.storage.get('token').then((data) => {
			if (data != null) {
				this.token = data;
				this.getStorages();
			}
		});
	}

	async getStorages() {
		await this.loading.open()
		await this.api.httpCall('getStorages', '', this.token).then(res => {
			this.loading.close();
			this.storages = res['data'];
		}, err => {
			this.loading.close()
			console.log(err)
		})
	}

	async updateLot() {
		let payload = {
			user_id: this.user.user_id,
			plant_id: this.user.plant.plant_id,
			trip_id: this.meta.trip_id,
			slip_id: this.meta.slip_id,
			storage_id: this.meta.storage_id,
			lot_id: this.meta.lot.lot_id,
			lot_no: this.meta.lot_no.replace(/\s/g, ""),
			loading_bags: this.meta.loading_bags,
			loading_quantity: this.meta.loading_quantity
		}
		await this.loading.open()
		await this.api.httpCall('updateLot', payload, this.token).then(res => {
			this.loading.close();
			this.loading.message('Lot Updated successfully.')
			this.modalCtrl.dismiss('update')
		}, err => {
			this.loading.close()
			console.log(err)
		})
	}

	checkLotNo(e) {
		let pattern = /[-’/`~!#*$@_%+=.,"^&(){}[\]|;:”<>?\\]/
		if (pattern.test(e.detail.value) == true) {
			this.meta.lot_no = this.meta.lot_no.replace(/[-’/`~!#*$@_%+=.,"^&(){}[\]|;:”<>?\\]/g, '').toUpperCase()
			this.valid_lot_no = false
			this.valid_lot_no_message = 'Lot number cannot contain special characters ~`! @#$%^&*()-_+={}[]|\;:"<>,./?'
		} else {
			this.meta.lot_no = this.meta.lot_no.toUpperCase()
			this.valid_lot_no_message = ''
		}
	}

	close() {
		this.modalCtrl.dismiss('close')
	}
}
