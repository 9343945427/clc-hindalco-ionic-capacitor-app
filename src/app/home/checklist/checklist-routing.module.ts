import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChecklistPage } from './checklist.page';
import { AuthGuard } from 'src/app/guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: ChecklistPage
  },
  {
    path: 'edit',
    loadChildren: () => import('./edit/edit.module').then( m => m.EditPageModule)
  },
  {
    path: 'create',
    loadChildren: () => import('./create/create.module').then( m => m.CreatePageModule)
  },
  {
    path: 'holdback',
    loadChildren: () => import('./holdback/holdback.module').then( m => m.HoldbackPageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'edit-packing',
    loadChildren: () => import('./edit-packing/edit-packing.module').then( m => m.EditPackingPageModule)
  },
  {
    path: 'edit-loading-type',
    loadChildren: () => import('./edit-loading-type/edit-loading-type.module').then( m => m.EditLoadingTypePageModule)
  },
  {
    path: 'edit-pallet',
    loadChildren: () => import('./edit-pallet/edit-pallet.module').then( m => m.EditPalletPageModule)
  },
  {
    path: 'edit-desiccant',
    loadChildren: () => import('./edit-desiccant/edit-desiccant.module').then( m => m.EditDesiccantPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChecklistPageRoutingModule {}
