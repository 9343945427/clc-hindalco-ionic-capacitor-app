import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Platform } from '@ionic/angular';
import { StatusBar, Style } from '@capacitor/status-bar';
@Component({
	selector: 'app-root',
	templateUrl: 'app.component.html',
	styleUrls: ['app.component.scss'],
})
export class AppComponent {
	constructor(
		private storage: Storage,
		private pltfrom : Platform
	) {
		this.storage.create()
	}

	async ngOnInit() {
		await this.pltfrom.ready().then(()=>{
			StatusBar.setBackgroundColor({ color: '#000d70' }).then(async () => {
				await StatusBar.setStyle({ style: Style.Default });
				await StatusBar.setOverlaysWebView({ overlay: false })
			}, err => {
				console.log(err)
			})
		})
	}
}
