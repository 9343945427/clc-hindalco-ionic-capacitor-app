import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InwardsPageRoutingModule } from './inwards-routing.module';

import { InwardsPage } from './inwards.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InwardsPageRoutingModule
  ],
  declarations: [InwardsPage]
})
export class InwardsPageModule {}
