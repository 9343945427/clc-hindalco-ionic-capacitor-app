import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, NavParams } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ApiService } from 'src/app/services/api.service';
import { LoadingService } from 'src/app/services/login.service';

@Component({
	selector: 'app-edit-lot',
	templateUrl: './edit-lot.page.html',
	styleUrls: ['./edit-lot.page.scss'],
})
export class EditLotPage implements OnInit {

	meta = {
		inspection_id: null,
		inspection_lot_id: null,
		pallet_size_id: null,
		lot_no: null,
		number_of_pallet: null
	}
	user: any;
	token: any;
	pallet_sizes: any;
	valid_lot_no: any;
	valid_lot_no_message = ''
	constructor(
		private storage: Storage,
		private api: ApiService,
		private loading: LoadingService,
		public router: Router,
		private navParams: NavParams,
		private modalCtrl: ModalController,
	) {
		console.log(this.navParams.data)
		this.meta.inspection_id = this.navParams.data['inspection_id']
		this.meta.inspection_lot_id = this.navParams.data['inspection_lot_id'];
		this.meta.lot_no = this.navParams.data['lot_no']
		this.meta.pallet_size_id = this.navParams.data['pallet_size_id']
		this.meta.number_of_pallet = this.navParams.data['number_of_pallet']
	}

	async ngOnInit() {

	}

	async ionViewWillEnter() {
		await this.storage.get('user').then((data) => {
			if (data != null) {
				this.user = data;
			}
		});
		await this.storage.get('token').then((data) => {
			if (data != null) {
				this.token = data;
				this.getPalletSizes()
			}
		});
	}

	async getPalletSizes() {
		await this.loading.open()
		await this.api.httpCall('getPalletSizes', '', this.token).then(res => {
			this.loading.close();
			this.pallet_sizes = res['data'];
		}, err => {
			this.loading.close()
			console.log(err)
		})
	}

	async updateLot() {
		let payload = {
			user_id: this.user.user_id,
			plant_id: this.user.plant.plant_id,
			inspection_id: this.meta.inspection_id,
			inspection_lot_id: this.meta.inspection_lot_id,
			pallet_size_id: this.meta.pallet_size_id,
			lot_no: this.meta.lot_no.replace(/\s/g, ""),
			number_of_pallet: this.meta.number_of_pallet
		}
		await this.loading.open()
		await this.api.httpCall('updateInspectionLot ', payload, this.token).then(res => {
			this.loading.close();
			this.loading.message('Lot deleted successfully.')
			this.modalCtrl.dismiss('update')
		}, err => {
			this.loading.close()
			console.log(err)
		})
	}

	checkLotNo(e) {
		let pattern = /[-’/`~!#*$@_%+=.,"^&(){}[\]|;:”<>?\\]/
		if (pattern.test(e.detail.value) == true) {
			this.meta.lot_no = this.meta.lot_no.replace(/[-’/`~!#*$@_%+=.,"^&(){}[\]|;:”<>?\\]/g, '').toUpperCase()
			this.valid_lot_no = false
			this.valid_lot_no_message = 'Lot number cannot contain special characters ~`! @#$%^&*()-_+={}[]|\;:"<>,./?'
		} else {
			this.meta.lot_no = this.meta.lot_no.toUpperCase()
			this.valid_lot_no_message = ''
		}
	}

	close() {
		this.modalCtrl.dismiss()
	}

}
