import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EditLotPage } from './edit-lot.page';

describe('EditLotPage', () => {
  let component: EditLotPage;
  let fixture: ComponentFixture<EditLotPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(EditLotPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
