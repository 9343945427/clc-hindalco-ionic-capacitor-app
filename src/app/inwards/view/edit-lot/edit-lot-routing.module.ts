import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditLotPage } from './edit-lot.page';

const routes: Routes = [
  {
    path: '',
    component: EditLotPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditLotPageRoutingModule {}
