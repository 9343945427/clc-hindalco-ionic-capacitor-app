import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditLotPageRoutingModule } from './edit-lot-routing.module';

import { EditLotPage } from './edit-lot.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditLotPageRoutingModule
  ],
  declarations: [EditLotPage]
})
export class EditLotPageModule {}
