import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewPage } from './view.page';

const routes: Routes = [
  {
    path: '',
    component: ViewPage
  },
  {
    path: 'add-lot',
    loadChildren: () => import('./add-lot/add-lot.module').then( m => m.AddLotPageModule)
  },
  {
    path: 'edit-lot',
    loadChildren: () => import('./edit-lot/edit-lot.module').then( m => m.EditLotPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewPageRoutingModule {}
