import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Camera, CameraDirection, CameraResultType, CameraSource } from '@capacitor/camera';
import { MediaCapture, MediaFile, CaptureError, CaptureVideoOptions } from '@awesome-cordova-plugins/media-capture/ngx';
import { ModalController, AlertController, Platform, ActionSheetController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { LoadingService } from 'src/app/services/login.service';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';
import { AddLotPage } from './add-lot/add-lot.page';
import { EditLotPage } from './edit-lot/edit-lot.page';
import { Filesystem } from '@capacitor/filesystem';
@Component({
	selector: 'app-view',
	templateUrl: './view.page.html',
	styleUrls: ['./view.page.scss'],
})
export class ViewPage implements OnInit {
	user: any;
	token: any;
	inspection: any
	segment = 'info'
	inspection_stages: any;
	inspection_checks: any;
	inspection_attachments: any
	inspection_check_attachments: any
	stage_name = ''
	current_index: any
	pending_stages = []
	constructor(
		private api: ApiService,
		private loading: LoadingService,
		private storage: Storage,
		private mediaCapture: MediaCapture,
		public router: Router,
		private route: ActivatedRoute,
		private modalCtrl: ModalController,
		public alertController: AlertController,
		private platform: Platform,
		private actionSheetController: ActionSheetController,
	) {
		this.route.queryParams.subscribe(params => {
			if (this.router.getCurrentNavigation().extras.state) {
				this.inspection = this.router.getCurrentNavigation().extras.state['inspection'];
			}
		});
		this.platform.backButton.subscribeWithPriority(10, (processNextHandler) => {
			// this.confirmSavedData()
		});
	}

	ngOnInit() {
	}

	async ionViewWillEnter() {
		await this.storage.get('user').then((data) => {
			if (data != null) {
				this.user = data;
			}
		});
		await this.storage.get('token').then((data) => {
			if (data != null) {
				this.token = data;
				this.getInspection()
			}
		});
		this.api.can_leave = false
	}

	async getInspection() {
		await this.loading.open()
		await this.api.httpCall('getInspection', { inspection_id: this.inspection.inspection_id }, this.token).then(res => {
			this.loading.close();
			this.inspection = res['data']
			this.inspection_stages = res['data'].inspection_stages
			this.pending_stages = this.inspection_stages.filter(pending_stage => {
				return (pending_stage.status == 0 || pending_stage.status == null)
			})
		}, err => {
			this.loading.close();
			console.log(err)
		})
	}

	async getPendingInspectionChecks(inspection_stage, i?) {
		let current_stage = inspection_stage
		let previous_stage = this.pending_stages.filter(stage => {
			return this.stage_name == stage.stage_name
		})
		if (previous_stage.length != 0) {
			if (current_stage.status == previous_stage[0].status) {
				if (current_stage.status == 0 && previous_stage[0].status == 0 && previous_stage[0].draft != 1) {
					alert('Please submit current stage')
					setTimeout(() => {
						this.segment = previous_stage[0].stage_name
						this.stage_name = this.segment
					}, 200);
				}
				else {
					console.log('179')
					let payload = {
						inspection_stage_id: inspection_stage.inspection_stage_id,
					}
					this.segment = this.inspection_stages.indexOf(inspection_stage)
					this.stage_name = inspection_stage.stage_name
					let stage = this.inspection_stages.filter(stage => {
						return stage.stage_name == this.stage_name
					})
					this.current_index = Math.abs(this.inspection_stages.indexOf(stage[0]))
					await this.loading.open()
					await this.api.httpCall('getInspectionChecks', payload, this.token).then(res => {
						this.loading.close();
						this.inspection_checks = res['data'];
						this.inspection_attachments = res['inspection_attachments']
						this.inspection_check_attachments = this.inspection_checks.map(ele => {
							return ele.inspection_attachments.map(inspection_attachment => {
								if (inspection_attachment.length != 0) {
									return inspection_attachment
								}
							})
						})
					}, err => {
						this.loading.close()
						console.log(err)
					})
				}
			}
			else if (this.pending_stages.length != 0 && i == this.inspection_stages.length - 1) {
				alert('Please complete all stages')
				setTimeout(() => {
					this.segment = 'info'
					this.stage_name = ''
				}, 200);
			}
			else if (current_stage.status == 1 && previous_stage[0].status == 0 && previous_stage[0].draft != 1) {
				alert('Please submit current stage')
				setTimeout(() => {
					this.segment = previous_stage[0].stage_name
					this.stage_name = this.segment
				}, 200);
			}
			else if (current_stage.status == 0 && previous_stage[0].status == 1 && previous_stage[0].draft != 1) {
				alert('Please submit current stage')
				setTimeout(() => {
					this.segment = previous_stage[0].stage_name
					this.stage_name = this.segment
				}, 200);
			}
			else {
				console.log('223')
				let payload = {
					inspection_stage_id: inspection_stage.inspection_stage_id,
				}
				this.segment = this.inspection_stages.indexOf(inspection_stage)
				this.stage_name = inspection_stage.stage_name
				await this.loading.open()
				await this.api.httpCall('getInspectionChecks', payload, this.token).then(res => {
					this.loading.close();
					this.inspection_checks = res['data'];
					this.inspection_attachments = res['inspection_attachments']
					this.inspection_check_attachments = this.inspection_checks.filter(ele => {
						if (ele.inspection_attachments.length != 0) return ele
						else return null
					})
					let stage = this.inspection_stages.filter(stage => {
						return stage.stage_name == this.stage_name
					})
					this.current_index = Math.abs(this.inspection_stages.indexOf(stage[0]))
				}, err => {
					this.loading.close()
					console.log(err)
				})
			}
		} else {
			if (this.pending_stages.length != 0 && i == this.inspection_stages.length - 1) {
				if (current_stage.inspection_stage_id == this.pending_stages[0].inspection_stage_id) {
					console.log(247)
					let payload = {
						inspection_stage_id: inspection_stage.inspection_stage_id,
					}
					this.segment = this.inspection_stages.indexOf(inspection_stage)
					this.stage_name = inspection_stage.stage_name
					let stage = this.inspection_stages.filter(stage => {
						return stage.stage_name == this.stage_name
					})
					this.current_index = Math.abs(this.inspection_stages.indexOf(stage[0]))
					await this.loading.open()
					await this.api.httpCall('getInspectionChecks', payload, this.token).then(res => {
						this.loading.close();
						this.inspection_checks = res['data'];
						this.inspection_attachments = res['inspection_attachments']
						this.inspection_check_attachments = this.inspection_checks.map(ele => {
							return ele.inspection_attachments.map(inspection_attachment => {
								if (inspection_attachment.length != 0) {
									return inspection_attachment
								}
							})
						})

					}, err => {
						this.loading.close()
						console.log(err)
					})
				}
				else {
					var drafted_stages = []
					console.log(current_stage)
					drafted_stages = this.pending_stages.filter(stage => {
						return stage.draft == 0 && current_stage.inspection_stage != stage.inspection_stage_id
					})
					if (drafted_stages[0]?.inspection_stage_id == current_stage.inspection_stage_id) drafted_stages = []
					if (drafted_stages.length == 0) {
						let payload = {
							inspection_stage_id: inspection_stage.inspection_stage_id,
						}
						this.segment = this.inspection_stages.indexOf(inspection_stage)
						this.stage_name = inspection_stage.stage_name
						await this.loading.open()
						await this.api.httpCall('getInspectionChecks', payload, this.token).then(res => {
							this.loading.close();
							this.inspection_checks = res['data'];
							this.inspection_attachments = res['inspection_attachments']
							this.inspection_check_attachments = this.inspection_checks.map(ele => {
								return ele.inspection_attachments.map(inspection_attachment => {
									if (inspection_attachment.length != 0) {
										return inspection_attachment
									}
								})
							})
							let stage = this.inspection_stages.filter(stage => {
								return stage.stage_name == this.stage_name
							})
							this.current_index = Math.abs(this.inspection_stages.indexOf(stage[0]))
						}, err => {
							this.loading.close()
							console.log(err)
						})
					} else {
						alert('Please complete all stages')
						setTimeout(() => {
							this.segment = 'info'
							this.stage_name = ''
						}, 200);
					}
				}
			} else {
				let payload = {
					inspection_stage_id: inspection_stage.inspection_stage_id,
				}
				this.segment = this.inspection_stages.indexOf(inspection_stage)
				this.stage_name = inspection_stage.stage_name
				await this.loading.open()
				await this.api.httpCall('getInspectionChecks', payload, this.token).then(res => {
					this.loading.close();
					this.inspection_checks = res['data'];
					this.inspection_attachments = res['inspection_attachments']
					this.inspection_check_attachments = this.inspection_checks.filter(ele => {
						if (ele.inspection_attachments.length != 0) return ele
						else return null
					})

					let stage = this.inspection_stages.filter(stage => {
						return stage.stage_name == this.stage_name
					})
					this.current_index = Math.abs(this.inspection_stages.indexOf(stage[0]))
				}, err => {
					this.loading.close()
					console.log(err)
				})
			}

		}
	}
	async addInspectionLot(inspection) {
		const modal = await this.modalCtrl.create({
			component: AddLotPage,
			componentProps: {
				inspection: inspection
			}
		});
		modal.present();
		modal.onDidDismiss().then((data) => {
			if (data.data == 'update') {
				this.getInspection();
			}
		})
	}

	async selectLotOption(inspection_lot) {
		const actionSheet = await this.actionSheetController.create({
			header: 'Select one option',
			buttons: [
				{
					text: 'Edit',
					icon: 'pencil-outline',
					handler: async () => {
						this.editLot(inspection_lot)
					}
				},
				{
					text: 'Delete',
					icon: 'trash-outline',
					handler: () => {
						this.confirmDelete(inspection_lot)
					}
				},
				{
					text: 'Cancel',
					icon: 'close-circle-outline',
					role: 'cancel'
				}
			]
		});
		await actionSheet.present();
	}

	async editLot(lot) {
		let lot_details = {
			inspection_id: lot.inspection_id,
			inspection_lot_id: lot.inspection_lot_id,
			user_id: this.user.user_id,
			plant_id: this.user.plant.plant_id,
			pallet_size_id: lot.pallet_size_id,
			lot_no: lot.lot_no,
			number_of_pallet: lot.number_of_pallet
		}
		const modal = await this.modalCtrl.create({
			component: EditLotPage,
			componentProps: lot_details
		});
		modal.present();
		modal.onDidDismiss().then(data => {
			if (data.data == 'update') {
				this.getInspection()
			}
		})
	}

	async deleteLot(inspection_lot) {
		let payload = {
			inspection_lot_id: inspection_lot.inspection_lot_id
		}
		await this.loading.open()
		await this.api.httpCall('deleteInspectionLot', payload, this.token).then(res => {
			this.loading.close();
			this.loading.message('Lot deleted successfully.')
			this.getInspection()
		}, err => {
			this.loading.close()
			console.log(err)
		})
	}

	async confirmDelete(inspection_lot) {
		const alert = await this.alertController.create({
			header: 'Confirm !',
			cssClass: "confirm-alert",
			message: 'Are you sure ?',
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel',
					id: 'cancel-button',
					handler: () => {
						return false
					}
				}, {
					text: 'Delete',
					id: 'confirm-button',
					handler: () => {
						this.deleteLot(inspection_lot)
					}
				}
			]
		});
		await alert.present();
	}

	async capturePhotos(inspection_checks) {
		if (inspection_checks.inspection_attachments) {
			let max_images = []
			max_images = inspection_checks.inspection_attachments.filter(img => {
				return img.type == 'image'
			})
			if (max_images.length == 15) {
				alert('Max 15 Images allowed')
			}
			else {
				const image = await Camera.getPhoto({
					quality: 50,
					allowEditing: false,
					resultType: CameraResultType.Base64,
					source: CameraSource.Camera,
					direction: CameraDirection.Rear
				});
				let imageUrl = 'data:image/jpeg;base64,' + image.base64String;
				let attachment_id = Math.floor(new Date().getTime())
				let attachment = {
					attachment: imageUrl,
					type: 'image',
					attachment_id: attachment_id
				}
				if (!inspection_checks.inspection_attachments) {
					inspection_checks.inspection_attachments = [];
				}
				inspection_checks.inspection_attachments.push(attachment)
			}
		} else {
			const image = await Camera.getPhoto({
				quality: 50,
				allowEditing: false,
				resultType: CameraResultType.Base64,
				source: CameraSource.Camera
			});
			let imageUrl = 'data:image/jpeg;base64,' + image.base64String;
			let attachment_id = Math.floor(new Date().getTime())
			let attachment = {
				attachment: imageUrl,
				type: 'image',
				attachment_id: attachment_id
			}
			if (!inspection_checks.inspection_attachments) {
				inspection_checks.inspection_attachments = [];
			}
			inspection_checks.inspection_attachments.push(attachment)
		}
	}

	async recordVideo(inspection_checks) {
		let vm = this
		let options: CaptureVideoOptions = {
			limit: 1,
			duration: 10,
			quality: 10
		}
		vm.mediaCapture.captureVideo(options).then(async (res: MediaFile[]) => {
			const contents = await Filesystem.readFile({
				path: res[0].fullPath,
			});
			let attachment = {
				attachment: 'data:video/mp4;base64,' + contents.data,
				type: 'video',
				attachment_id: Math.floor(new Date().getTime())
			}
			if (!inspection_checks.inspection_attachments) {
				inspection_checks.inspection_attachments = [];
			}
			inspection_checks.inspection_attachments.push(attachment)
		}, (err: CaptureError) => console.error(err));
	}

	deleteAttachment(inspection_checks, attachment_id) {
		// inspection_checks.inspection_attachments = inspection_checks.inspection_attachments.filter(attachment => {
		// 	return attachment.attachment_id !== attachment_id
		// });

		inspection_checks.inspection_attachments = inspection_checks.inspection_attachments.filter(attachment => {
			if (!attachment.check_id) {
				return attachment.attachment_id !== attachment_id
			}
			else {
				let inspection_check = inspection_checks.filter(ele => {
					return ele.inspection_check_id == attachment.check_id
				})
				console.log(inspection_check)
				inspection_check[0].value = ''
				return attachment.attachment_id !== attachment_id
			}

		});
	}

	async selectMedia(inspection_checks) {
		const actionSheet = await this.actionSheetController.create({
			header: 'What would you like to add?',
			buttons: [
				{
					text: 'Capture Image',
					icon: 'camera-outline',
					handler: () => {
						this.capturePhotos(inspection_checks);
					}
				},
				{
					text: 'Capture Video',
					icon: 'videocam-outline',
					handler: () => {
						this.recordVideo(inspection_checks);
					}
				},
				{
					text: 'Cancel',
					icon: 'close-circle-outline',
					role: 'cancel'
				}
			]
		});
		await actionSheet.present();
	}

	convertDate(date) {
		return moment(date).format('DD/MM/YYYY');
	}

	async checkSegment(e) {
		this.segment == 'info' ? this.stage_name = this.segment : 0
		this.stage_name = this.segment
	}

	checkColor(inspection_check) {
		if (inspection_check.min == '' || inspection_check.max == '' || !inspection_check.min || !inspection_check.max || inspection_check.value == '' || inspection_check.value == '') {
			return false;
		}
		if (inspection_check.value < inspection_check.min || inspection_check.value > inspection_check.max) {
			return true;
		}
		return false;
	}

	async handleChange(e, inspection_check, inspection_checks) {
		let value = ''
		if (!inspection_checks.inspection_attachments) {
			inspection_checks.inspection_attachments = [];
		}
		if (!inspection_check.attachments) {
			inspection_check.attachments = [];
		}
		if (inspection_check.is_image_required == 1) {
			value = e.detail.value.toLowerCase()
			if (value == 'yes') {
				await this.platform.ready().then(async res => {
					const image = await Camera.getPhoto({
						quality: 50,
						allowEditing: false,
						resultType: CameraResultType.Base64,
						source: CameraSource.Camera,
						direction: CameraDirection.Rear
					}).then(async res => {
						let imageUrl = 'data:image/jpeg;base64,' + res.base64String;
						let attachment_id = Math.floor(new Date().getTime())
						let attachment = {
							attachment: imageUrl,
							type: 'image',
							attachment_id: attachment_id,
							check_id: inspection_check.check_id,
							inspection_check_id: inspection_check.inspection_check_id
						}
						if (!inspection_checks.inspection_attachments) {
							inspection_checks.inspection_attachments = [];
						}
						if (!inspection_check.attachments) {
							inspection_check.attachments = [];
						}
						inspection_checks.inspection_attachments.push(attachment)
						inspection_check.attachments.push(attachment)
						inspection_check.value = e.detail.value
					}, err => {
						console.log(err)
						alert('Image is compulsory for this check')
						inspection_check.value = ''
					});
				})
			} else {
				this.confirmTripCheckValue(inspection_check, e.detail.value, inspection_checks)
			}
		}
		else {
			if (!inspection_check.attachments) {
				inspection_check.attachments = [];
			}
			value = e.detail.value.toLowerCase()
			if (value == 'yes') {
				inspection_check.value = e.detail.value
			} else {
				this.confirmTripCheckValue(inspection_check, e.detail.value, inspection_checks)
			}

		}
	}

	async confirmTripCheckValue(inspection_check, value, inspection_checks) {
		const alert = await this.alertController.create({
			header: 'Confirm !',
			cssClass: 'confirm-alert',
			message: 'Are you sure? If Yes, then please add Comments in the Additional Comments field at the bottom. ',
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel',
					id: 'cancel-button',
					handler: () => {
						inspection_check.value = ''
					}
				}, {
					text: 'Continue',
					id: 'confirm-button',
					handler: async () => {
						await this.platform.ready().then(async res => {
							const image = await Camera.getPhoto({
								quality: 50,
								allowEditing: false,
								resultType: CameraResultType.Base64,
								source: CameraSource.Camera,
								direction: CameraDirection.Rear
							}).then(async res => {
								let imageUrl = 'data:image/jpeg;base64,' + res.base64String;
								let attachment_id = Math.floor(new Date().getTime())
								let attachment = {
									attachment: imageUrl,
									type: 'image',
									attachment_id: attachment_id,
									check_id: inspection_check.check_id,
									inspection_check_id: inspection_check.inspection_check_id
								}
								if (!inspection_checks.inspection_attachments) {
									inspection_checks.inspection_attachments = [];
								}
								if (!inspection_check.attachments) {
									inspection_check.attachments = [];
								}
								inspection_checks.inspection_attachments.push(attachment)
								inspection_check.attachments.push(attachment)
								inspection_check.value = value
							}, err => {
								console.log(err)
								this.loading.presentAlert('Alert', 'Image is compulsory for this check')
								inspection_check.value = ''
							});
						})

					}
				}
			]
		});
		await alert.present();
	}

	async confirmSubmitInspection(inspection_checks, inspection_stages, draft) {
		let empty_fields = []
		for (let inspection_check of inspection_checks) {
			if (inspection_check.value == '' || inspection_check.value == null || !inspection_check.value) {
				empty_fields.push(inspection_check)
			}
		}
		if (empty_fields.length !== 0) {
			this.loading.presentAlert('Alert', 'All fields are mandatory. Please fill all fields in order to submit.')
		}
		else {
			const alert = await this.alertController.create({
				header: 'Confirm !',
				cssClass: 'confirm-alert',
				message: 'You are about to submit last stage of the Inspection. Please make sure all stages are submitted and all checks are answered. Any unsaved data will be lost!',
				buttons: [
					{
						text: 'Cancel',
						role: 'cancel',
						id: 'cancel-button',
						handler: () => {
							return false
						}
					}, {
						text: 'Continue',
						id: 'confirm-button',
						handler: () => {
							this.submitChecks(inspection_checks, inspection_stages, 0)
						}
					}
				]
			});
			await alert.present();
		}
	}

	async submitChecks(inspection_checks, inspection_stages, draft) {
		let payload = {
			inspection_checks: inspection_checks,
			plant_id: inspection_checks[0]?.plant_id,
			inspection_id: inspection_checks[0]?.inspection_id,
			inspection_stage_id: inspection_checks[0]?.inspection_stage_id,
			attachments: inspection_checks['inspection_attachments'],
			inspection_lots: this.inspection.inspection_lots,
			note: null,
			draft: draft
		}
		let note = inspection_stages.filter((inspection_stage) => {
			return (inspection_stage.inspection_stage_id == payload.inspection_stage_id)
		})
		payload.note = note[0]?.note
		let empty_note = []
		for (let inspection_check of payload.inspection_checks) {
			let empty_value = inspection_check.value.toString().toLowerCase()
			if (empty_value == 'no' && payload.note == null || payload.note == '') {
				empty_note.push(inspection_check)
			}
		}
		if (empty_note.length !== 0) {
			this.loading.presentAlert('Alert', 'We have discovered some deviated checks. Please provide additional comments to continue..')
		} else {
			await this.loading.open()
			await this.api.httpCall('updateInspectionCheck', payload, this.token).then(async res => {
				this.loading.close();
				this.pending_stages = res['pending_inspection_stages']
				this.loading.message('Checks updated successfully.')
				let index: any
				index = inspection_stages.map(e => e.inspection_stage_id).indexOf(payload.inspection_stage_id);
				if (index == inspection_stages.length - 1) {
					this.router.navigate(['/tabs/inwards'])

				}
				else {
					let inspection_stage = inspection_stages[index + 1]
					this.segment = inspection_stage.stage_name
					this.stage_name = inspection_stage.stage_name
					this.current_index = Math.abs(this.inspection_stages.indexOf(inspection_stage))
					this.getInspectionChecks(inspection_stage)
				}
			}, err => {
				this.loading.close();
				console.log(err)
			})

		}
	}

	async getInspectionChecks(inspection_stage, i?) {
		let payload = {
			inspection_stage_id: inspection_stage.inspection_stage_id,
		}
		this.segment = this.inspection_stages.indexOf(inspection_stage)
		this.stage_name = inspection_stage.stage_name
		await this.loading.open()
		await this.api.httpCall('getInspectionChecks', payload, this.token).then(res => {
			this.loading.close();
			this.inspection_checks = res['data'];
			this.inspection_attachments = res['inspection_attachments']
			this.inspection_check_attachments = this.inspection_checks.map(ele => {
				return ele.inspection_attachments.map(inspection_attachment => {
					if (inspection_attachment.length != 0) {
						return inspection_attachment
					}
				})
			})
		}, err => {
			this.loading.close()
			console.log(err)
		})
	}

	check(attachment) {
		console.log(attachment)
		return attachment

	}
}
