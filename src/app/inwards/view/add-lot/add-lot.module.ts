import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddLotPageRoutingModule } from './add-lot-routing.module';

import { AddLotPage } from './add-lot.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddLotPageRoutingModule
  ],
  declarations: [AddLotPage]
})
export class AddLotPageModule {}
