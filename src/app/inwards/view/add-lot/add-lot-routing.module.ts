import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddLotPage } from './add-lot.page';

const routes: Routes = [
  {
    path: '',
    component: AddLotPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddLotPageRoutingModule {}
