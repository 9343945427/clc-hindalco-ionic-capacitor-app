import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AddLotPage } from './add-lot.page';

describe('AddLotPage', () => {
  let component: AddLotPage;
  let fixture: ComponentFixture<AddLotPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(AddLotPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
