import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';
import { ModalController } from '@ionic/angular';
import { CreatePage } from './create/create.page';

@Component({
	selector: 'app-inwards',
	templateUrl: './inwards.page.html',
	styleUrls: ['./inwards.page.scss'],
})
export class InwardsPage {
	user: any;
	token: any;
	meta = {
		keyword: "inspection_id",
		search: "",
		order_by: "DESC",
		per_page: 10,
		type: "new",
		status: '',
		page: 1,
		last_page: 0,
	}
	inspections: any;
	constructor(
		private api: ApiService,
		private storage: Storage,
		public router: Router,
		private modalCtrl: ModalController
	) { }

	async ionViewWillEnter() {
		this.meta.type = 'new';
		this.meta.page = 1;
		this.meta.last_page = 0
		this.inspections = null;
		await this.storage.get('user').then((data) => {
			if (data != null) {
				this.user = data;
			}
		});
		await this.storage.get('token').then((data) => {
			if (data != null) {
				this.token = data;
				this.meta.page = 1;
				this.meta.last_page = 0;
				this.meta.type = 'new';
				this.paginateInspections();
			}
		});
	}

	async paginateInspections(e?) {
		let payload = {
			keyword: this.meta.keyword,
			search: this.meta.search,
			order_by: this.meta.order_by,
			per_page: this.meta.per_page,
			type: this.meta.type,
			status: this.meta.status,
			page: this.meta.page,
			last_page: this.meta.last_page,
		}
		await this.api.httpCall('paginateInspections', payload, this.token).then(res => {
			this.meta.last_page = res['meta'].last_page;
			if (this.meta.type == 'new')
				this.inspections = res['data']
			else
				this.inspections = this.inspections.concat(res['data']);
			if (e)
				e.target.complete();
		}, err => {
			e.target.complete();
			console.log(err)
		})
	}

	async loadData(event) {
		this.meta.type = 'append';
		this.meta.page++;
		await this.paginateInspections(event);
	}

	async doRefresh(event) {
		this.meta.search = ''
		this.meta.type = 'new';
		this.meta.page = 1;
		this.meta.last_page = 0
		this.inspections = null;
		await this.paginateInspections(event);
	}

	async searchData(event) {
		if (event.key == 'Enter' || event.target.value == '') {
			this.meta.type = 'new';
			this.meta.page = 1;
			this.meta.last_page = 0;
			this.meta.search = event.target.value;
			await this.paginateInspections();
		}
	}

	async addInspection() {
		const modal = await this.modalCtrl.create({
			component: CreatePage,
		});
		modal.present();
		modal.onDidDismiss().then((data) => {
			if (data.data) {
				this.meta.search = ''
				this.meta.type = 'new';
				this.meta.page = 1;
				this.meta.last_page = 0
				this.inspections = null;
				this.paginateInspections()
			}
		})
	}

	viewInspection(inspection) {
		let navigationExtras: NavigationExtras = {
			state: {
				inspection: inspection
			}
		}
		this.router.navigate(['/tabs/inwards/view'], navigationExtras)
	}

	convertDate(date) {
		return moment(date).format('DD/MM/YYYY');
	}

}
