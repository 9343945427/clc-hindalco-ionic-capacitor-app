import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InwardsPage } from './inwards.page';

const routes: Routes = [
  {
    path: '',
    component: InwardsPage
  },
  {
    path: 'view',
    loadChildren: () => import('./view/view.module').then( m => m.ViewPageModule)
  },
  {
    path: 'create',
    loadChildren: () => import('./create/create.module').then( m => m.CreatePageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InwardsPageRoutingModule {}
