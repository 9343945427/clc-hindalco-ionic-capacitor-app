import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InwardsPage } from './inwards.page';

describe('InwardsPage', () => {
  let component: InwardsPage;
  let fixture: ComponentFixture<InwardsPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(InwardsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
