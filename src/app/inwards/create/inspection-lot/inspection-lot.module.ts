import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InspectionLotPageRoutingModule } from './inspection-lot-routing.module';

import { InspectionLotPage } from './inspection-lot.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InspectionLotPageRoutingModule
  ],
  declarations: [InspectionLotPage]
})
export class InspectionLotPageModule {}
