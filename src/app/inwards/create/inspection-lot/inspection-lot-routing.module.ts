import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InspectionLotPage } from './inspection-lot.page';

const routes: Routes = [
  {
    path: '',
    component: InspectionLotPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InspectionLotPageRoutingModule {}
