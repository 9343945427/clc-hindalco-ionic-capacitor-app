import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, NavParams } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { LoadingService } from 'src/app/services/login.service';
import { Storage } from '@ionic/storage';
@Component({
	selector: 'app-inspection-lot',
	templateUrl: './inspection-lot.page.html',
	styleUrls: ['./inspection-lot.page.scss'],
})
export class InspectionLotPage implements OnInit {
	meta = {
		inspection_id: null,
		pallet_size_id: null,
		lot_no: null,
		number_of_pallet: null,
		inspection_lot_id: "",
		pallet_size: {
			pallet_size_name: ''
		},
		lot_id: ""
	}
	user: any;
	token: any;
	pallet_sizes: any;
	valid_lot_no: any;
	valid_lot_no_message = ''
	constructor(
		private storage: Storage,
		private api: ApiService,
		private loading: LoadingService,
		public router: Router,
		private navParams: NavParams,
		private modalCtrl: ModalController,
	) {
	}

	async ngOnInit() {

	}

	async ionViewWillEnter() {
		await this.storage.get('token').then((data) => {
			if (data != null) {
				this.token = data;
				this.getPalletSizes()
			}
		});
	}

	async getPalletSizes() {
		await this.loading.open()
		await this.api.httpCall('getPalletSizes', '', this.token).then(res => {
			this.loading.close();
			this.pallet_sizes = res['data'];
		}, err => {
			this.loading.close()
			console.log(err)
		})
	}

	async createLot() {
		console.log(this.meta)
		let inspection_lots = {
			lot_id: this.meta.lot_id,
			inspection_id: this.meta.inspection_id,
			pallet_size_id: this.meta.pallet_size_id,
			pallet_size: {
				pallet_size_name: this.meta.pallet_size.pallet_size_name
			},
			lot_no: this.meta.lot_no.replace(/\s/g, ""),
			number_of_pallet: this.meta.number_of_pallet
		}

		this.modalCtrl.dismiss(inspection_lots)
	}

	checkLotNo(e) {
		let pattern = /[-’/`~!#*$@_%+=.,"^&(){}[\]|;:”<>?\\]/
		if (pattern.test(e.detail.value) == true) {
			this.meta.lot_no = this.meta.lot_no.replace(/[-’/`~!#*$@_%+=.,"^&(){}[\]|;:”<>?\\]/g, '').toUpperCase()
			this.valid_lot_no = false
			this.valid_lot_no_message = 'Lot number cannot contain special characters ~`! @#$%^&*()-_+={}[]|\;:"<>,./?'
		} else {
			this.meta.lot_no = this.meta.lot_no.toUpperCase()
			this.valid_lot_no_message = ''
		}
	}

	handlePallet(e) {
		let pallet_size_name
		pallet_size_name = this.pallet_sizes.filter(ele => {
			return ele.pallet_size_id == e.detail.value
		})
		this.meta.pallet_size.pallet_size_name = pallet_size_name[0].pallet_size_name
	}

	close() {
		this.modalCtrl.dismiss()
	}

}
