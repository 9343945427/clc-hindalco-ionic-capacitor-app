import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InspectionLotPage } from './inspection-lot.page';

describe('InspectionLotPage', () => {
  let component: InspectionLotPage;
  let fixture: ComponentFixture<InspectionLotPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(InspectionLotPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
