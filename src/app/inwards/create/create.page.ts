import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActionSheetController, AlertController, ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ApiService } from 'src/app/services/api.service';
import { LoadingService } from 'src/app/services/login.service';
import * as moment from 'moment';
import { InspectionLotPage } from './inspection-lot/inspection-lot.page';
@Component({
	selector: 'app-create',
	templateUrl: './create.page.html',
	styleUrls: ['./create.page.scss'],
})
export class CreatePage implements OnInit {
	meta = {
		deleted_inspection_lots: [],
		inspection_id: "",
		wagon_no: "",
		receipt_no: '',
		receipt_date_time:'',
		supplier_id: "",
		inspection_lots: [],
		note: ''
	}
	suppliers: any;
	valid_wagon_no: any;
	valid_wagon_no_message = ''
	user: any;
	token: any;
	errors:any
	constructor(
		private storage: Storage,
		private api: ApiService,
		private loading: LoadingService,
		public router: Router,
		private modalCtrl: ModalController,
		public actionSheetController: ActionSheetController,
	) {
	}

	async ngOnInit() {

	}

	async ionViewWillEnter() {
		await this.storage.get('token').then((data) => {
			if (data != null) {
				this.token = data;
				this.meta.receipt_date_time =  moment(new Date()).format('DD/MM/YYYY')
				this.generateReceiptNo()
			}
		});
	}

	async generateReceiptNo() {
		await this.loading.open()
		await this.api.httpCall('generateReceiptNo', '', this.token).then(res => {
			this.loading.close();
			let receipt_no:any;
			receipt_no=res
			this.meta.receipt_no =receipt_no
			this.getSuppliers()
		}, err => {
			this.loading.close()
			console.log(err)
		})
	}

	async getSuppliers() {
		await this.loading.open()
		await this.api.httpCall('getSuppliers', '', this.token).then(res => {
			this.loading.close();
			this.suppliers = res['data']
		}, err => {
			this.loading.close()
			console.log(err)
		})
	}

	async addInspection() {
		await this.loading.open()
		await this.api.httpCall('addInspection', this.meta, this.token).then(res => {
			this.loading.close();
			this.loading.message('New Inspection added successfully.')
			this.modalCtrl.dismiss(this.meta)
		}, err => {
			this.loading.close()
			console.log(err)
			this.errors = err.message
		})
	}

	async addInspectionLots() {
		const modal = await this.modalCtrl.create({
			component: InspectionLotPage,
		})
		modal.present();
		modal.onDidDismiss().then((data) => {
			console.log(data)
			if (data.data) {
				this.meta.inspection_lots.push(data.data)
			}
		})
	}

	checkWagonNo(e) {
		this.meta.wagon_no = this.meta.wagon_no.replace(/\s/g, "").toUpperCase();
		let pattern = /[-’/`~!#*$₹@_%+=.,"^&(){}[\]|;:”<>?\\]/
		if (pattern.test(this.meta.wagon_no) == true) {
			this.meta.wagon_no = this.meta.wagon_no.replace(/[-’/`~!#*$@_%+=.,"^&(){}[\]|;:”<>?\\]/g, '').toUpperCase()
			this.valid_wagon_no = false
			this.valid_wagon_no_message = 'Wagon number cannot contain special characters ~`! @#$%^&*()-_+={}[]|\;:"<>,./?'
		} else {
			this.valid_wagon_no_message = ''
		}
	}

	async confirmSelection(inspection_lot) {
		const actionSheet = await this.actionSheetController.create({
			header: 'What would you like to add?',
			buttons: [
				{
					text: 'Cancel',
					icon: 'close-circle-outline',
					role: 'cancel'
				},
				{
					text: 'Delete',
					icon: 'trash-outline',
					handler: () => {
						this.deleteInspectionLot(inspection_lot);
					}
				},

			]
		});
		await actionSheet.present();
	}

	deleteInspectionLot(inspection_lot) {
		this.meta.inspection_lots = this.meta.inspection_lots.filter(el => el.lot_no != inspection_lot.lot_no);
	}

	convertDate(e, receipt_date_time) {
		this.meta.receipt_date_time = moment(e.detail.value).format('DD/MM/YYYY');
	}

	close() {
		this.modalCtrl.dismiss()
	}

}
