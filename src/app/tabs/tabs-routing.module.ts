import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';
import { CanLeaveGuard } from '../guards/can-leave.guard';

const routes: Routes = [
	{
		path: '',
		component: TabsPage,
		children: [
			{
				path: 'home',
				loadChildren: () => import('../home/home.module').then(m => m.HomePageModule)
			},
			{
				path: 'account',
				loadChildren: () => import('../account/account.module').then(m => m.AccountPageModule)
			},

			{
				path: 'trips',
				loadChildren: () => import('../trips/trips.module').then(m => m.TripsPageModule)
			},
			{
				path: 'inwards',
				loadChildren: () => import('../inwards/inwards.module').then(m => m.InwardsPageModule)
			},
			{
				path: '',
				redirectTo: '/tabs/home',
				pathMatch: 'full'
			}
		]
	},
	{
		path: '',
		redirectTo: '/tabs/home',
		pathMatch: 'full'
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule { }
