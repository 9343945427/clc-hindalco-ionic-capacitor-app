import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { AlertController, ModalController, NavController } from '@ionic/angular';
import { ApiService } from '../services/api.service';

@Injectable({
	providedIn: 'root'
})
export class CanLeaveGuard implements CanDeactivate<unknown> {
	can_leave = false;
	constructor(
		public navCtrl: NavController,
		public alertController: AlertController,
		private api: ApiService,
		private modalCtrl: ModalController
	) {
	}
	async canDeactivate() {
		if (this.api.can_leave == false) {
			const alert = await this.alertController.create({
				header: 'Confirm !',
				cssClass: 'confirm-alert',
				backdropDismiss: false,
				message: 'Please make sure all data is Drafted / Submitted and all checks are answered with respective Images. Any unsaved data will be lost!',
				buttons: [
					{
						text: 'Cancel',
						role: 'cancel',
						id: 'cancel-button',
						handler: () => {
							this.api.can_leave = false
						}
					}, {
						text: 'Continue',
						id: 'confirm-button',
						handler: () => {
							this.modalCtrl.dismiss('')
							this.api.can_leave = true
						}
					}
				]
			});
			await alert.present();
			return await alert.onDidDismiss().then(() => {
				return this.api.can_leave
			})
		} else {
			return this.api.can_leave
		}
	}
}
