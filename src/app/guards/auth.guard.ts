import { Injectable } from '@angular/core';
import { CanLoad } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LoadingService } from '../services/login.service';

@Injectable({
	providedIn: 'root'
})
export class AuthGuard implements CanLoad {

	constructor(
		public navCtrl: NavController,
		private storage: Storage,
		private loading : LoadingService
	) {
	}

	async canLoad() {
		let state = 0;
		await this.storage.get('user').then((data) => {
			if (data != null) {
				state = 1;
			}
		});
		if (state == 0) {
			this.navCtrl.navigateRoot('/login');
			this.loading.message('User logged out. Please login.')
			return false;
		}
		else {
			return true;
		}
	}
}
