import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { LoadingService } from 'src/app/services/login.service';
import { Storage } from '@ionic/storage';
import { Camera, CameraResultType, CameraSource } from '@capacitor/camera';
import { Filesystem, Directory, Encoding } from '@capacitor/filesystem';
@Component({
	selector: 'app-profile',
	templateUrl: './profile.page.html',
	styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
	profileForm: FormGroup;
	errors: any;
	user: any;
	token: ""
	validations = {
		'name': [
			{ type: 'required', message: 'Name field is required.' }
		],
		'username': [
			{ type: 'required', message: 'Username field is required.' }
		],
		'mobile_no': [
			{ type: 'required', message: 'Mobile number field is required.' },
			{ type: 'pattern', message: 'Enter a valid mobile number.' },
			{ type: 'maxlength', message: 'Enter a valid mobile number.' }
		],
		'email': [
			{ type: 'required', message: 'Email field is required.' },
			{ type: 'pattern', message: 'Enter a valid email.' }
		],
		'address': [
			{ type: 'required', message: 'City field is required.' }
		],
	};

	constructor(
		private storage: Storage,
		public loading: LoadingService,
		public api: ApiService,
		private navCtrl: NavController,
	) {
		this.profileForm = new FormGroup({
			'name': new FormControl('', Validators.compose([
				Validators.required,
			])),
			'username': new FormControl('', Validators.compose([
				Validators.required,
			])),
			'mobile_no': new FormControl('', Validators.compose([
				Validators.required,
				Validators.pattern('^([0|\+[0-9]{1,5})?([6-9][0-9]{9})$'),
				Validators.maxLength(10)
			])),
			'email': new FormControl('', Validators.compose([
				Validators.required,
				Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
			])),
			'address': new FormControl('Karnataka', Validators.compose([
				Validators.required,
			])),
			'role': new FormControl('Karnataka', Validators.compose([
				Validators.required,
			])),
			'avatar': new FormControl(''),
		});
	}

	ngOnInit() {
	}

	async ionViewWillEnter() {
		await this.storage.get('user').then((data) => {
			if (data != null) {
				this.user = data;
				this.profileForm.controls["username"].setValue(this.user.username);
				this.profileForm.controls["name"].setValue(this.user.name);
				this.profileForm.controls["email"].setValue(this.user.email);
				this.profileForm.controls["mobile_no"].setValue(this.user.mobile_no);
				this.profileForm.controls["address"].setValue(this.user.address);
				this.profileForm.controls["role"].setValue(this.user.role.role_name);
				this.profileForm.controls["avatar"].setValue(this.user.avatar);
			}
		});
		await this.storage.get('token').then((data) => {
			if (data != null) {
				this.token = data;
			}
		});
	}

	async capturePhotos() {
		const image = await Camera.getPhoto({
			quality: 10,
			allowEditing: false,
			resultType: CameraResultType.Base64,
			source: CameraSource.Prompt,
		});
		let imageUrl = 'data:image/jpeg;base64,' + image.base64String;
		document.getElementById('image').setAttribute('src',imageUrl) 
		this.profileForm.controls['avatar'].setValue(imageUrl);
	}
	
	async updateProfile() {
		await this.loading.open();
		let formData = new FormData();
		formData.append("username", this.profileForm.controls["username"].value);
		formData.append("name", this.profileForm.controls["name"].value);
		formData.append("email", this.profileForm.controls["email"].value);
		formData.append("mobile_no", this.profileForm.controls["mobile_no"].value);
		formData.append("address", this.profileForm.controls["address"].value);
		formData.append("avatar", this.profileForm.controls["avatar"].value);
		await this.api.httpCall('updateUserProfile', formData, this.token).then((response) => {
			this.loading.close();
			this.storage.set('user', response['user']);
			this.loading.message('Profile is updated successfully ');
			this.navCtrl.navigateRoot('tabs/account');
		}, (error) => {
			this.loading.close();
			this.errors = error.errors;
		});
	}
}