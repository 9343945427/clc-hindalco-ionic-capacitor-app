import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { LoadingService } from '../services/login.service';
import { Storage } from '@ionic/storage';
@Component({
	selector: 'app-account',
	templateUrl: './account.page.html',
	styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {
	user: any;
	token: any;
	constructor(
		public api: ApiService,
		public loading: LoadingService,
		private storage: Storage,
		private navCtrl: NavController,
	) { }

	async ngOnInit() {
		await this.storage.get('user').then((data) => {
			if (data != null) {
				this.user = data;
			}
		});
		await this.storage.get('token').then((data) => {
			if (data != null) {
				this.token = data;
			}
		});
	}

	async logout() {
		await this.loading.open()
		await this.api.httpCall('logout', this.user, this.token).then(() => {
			this.loading.close()
			this.storage.remove('user');
			this.storage.remove('token');
			this.navCtrl.navigateRoot('/login');
		}, err => {
			this.loading.close()
			console.log(err)
			this.storage.remove('user');
			this.storage.remove('token');
			this.navCtrl.navigateRoot('/login');
		})
	}
}
