import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms'
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ApiService } from 'src/app/services/api.service';
import { LoadingService } from 'src/app/services/login.service';

@Component({
	selector: 'app-password',
	templateUrl: './password.page.html',
	styleUrls: ['./password.page.scss'],
})
export class PasswordPage implements OnInit {
	passwordForm: FormGroup;
	errors: any;
	user: any;
	token = '';
	validations = {
		'current_password': [
			{ type: 'required', message: 'Current Password field is required.' },
			{ type: 'minlength', message: 'Current Password must be at least 6 characters long.' }
		],
		'new_password': [
			{ type: 'required', message: 'New Password field is required.' },
			{ type: 'minlength', message: 'New Password must be at least 6 characters long.' }
		],
		'confirm_password': [
			{ type: 'required', message: 'Confirm Password field is required.' },
			{ type: 'minlength', message: 'Confirm Password must be at least 6 characters long.' }
		],
	};
	constructor(
		public loading: LoadingService,
		public api: ApiService,
		private navCtrl: NavController,
		private storage: Storage,
	) {
		this.passwordForm = new FormGroup({
			'user_id': new FormControl('', Validators.compose([
				Validators.required
			])),
			'current_password': new FormControl('', Validators.compose([
				Validators.minLength(6),
				Validators.required
			])),
			'new_password': new FormControl('', Validators.compose([
				Validators.minLength(6),
				Validators.required
			])),
			'confirm_password': new FormControl('', Validators.compose([
				Validators.minLength(6),
				Validators.required
			])),
		});
	}

	ngOnInit() {
	}

	async ionViewDidEnter() {
		await this.storage.get('user').then((data) => {
			if (data != null) {
				this.user = data;
				this.passwordForm.controls["user_id"].setValue(this.user?.user_id);
			}
		});
		await this.storage.get('token').then((data) => {
			if (data != null) {
				this.token = data;
			}
		});
	}

	async changePassword() {
		await this.loading.open();
		await this.api.httpCall('changepassword', this.passwordForm.value, this.token).then((response) => {
			this.loading.close();
			this.navCtrl.navigateRoot('/tabs/account');
			this.loading.message('Password is successfully updated');
		}, (error) => {
			this.errors = error.errors;
			this.loading.close();
		});
	}
}
