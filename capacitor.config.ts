import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.akxatech.clic',
  appName: 'clic',
  webDir: 'www',
  // server: {
  //   androidScheme: 'https'
  // }
};

export default config;
